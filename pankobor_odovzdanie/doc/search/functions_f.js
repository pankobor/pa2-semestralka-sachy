var searchData=
[
  ['tcoord',['TCoord',['../structTCoord.html#ac743d5fca4f2565daeb0442605ee27ef',1,'TCoord::TCoord(const char &amp;c, const char &amp;r)'],['../structTCoord.html#a5ae83215a5e59b68bc6fb4bb52869c65',1,'TCoord::TCoord(const char *src)']]],
  ['testcheck',['testCheck',['../classCBoard.html#a9b332e0a336421ab57bb047f298443c9',1,'CBoard::testCheck(const TCoord &amp;target_field, bool white) const'],['../classCBoard.html#a46467dd2e8091b2ec6fbb9d9b872327c',1,'CBoard::testCheck(bool white) const']]],
  ['testcheckmate',['testCheckmate',['../classCBoard.html#a351918fba1ae433a3b68a4882a663949',1,'CBoard']]],
  ['testend',['testEnd',['../classCBoard.html#a49a1218930b4cf24a90fa49dae0ab83f',1,'CBoard']]],
  ['testpath',['testPath',['../classCBoard.html#a94a423316d2531863ce9284aad90390f',1,'CBoard']]],
  ['teststalemate',['testStalemate',['../classCBoard.html#af139eb90c3d02e8cda566f83966b5f2e',1,'CBoard']]],
  ['tostring',['toString',['../structTCoord.html#ac3aa5a1e3b713b5fa07e615e827ac52c',1,'TCoord']]],
  ['trycastling',['tryCastling',['../classCBoard.html#a2c089a131a65d8a4116cf0817bd64ec4',1,'CBoard']]],
  ['tryenpassant',['tryEnPassant',['../classCBoard.html#af1f407cd7af0d6a2910ca655063c9c86',1,'CBoard']]],
  ['trypromotion',['tryPromotion',['../classCBoard.html#a26905798611c3d4c59d26e55c3b1de56',1,'CBoard']]]
];
