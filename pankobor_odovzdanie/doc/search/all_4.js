var searchData=
[
  ['file_5fexamples',['FILE_EXAMPLES',['../constants_8h.html#aa625a8d1f618b1b97137a6c8f80db6eb',1,'constants.h']]],
  ['file_5fsaves',['FILE_SAVES',['../constants_8h.html#aeffb88ab995793f4da6bd8a4336a8074',1,'constants.h']]],
  ['flag_5fcheck',['FLAG_CHECK',['../constants_8h.html#a36b75425e4f18ac8539d8a1c7ae1c642',1,'constants.h']]],
  ['flag_5fcheckmate',['FLAG_CHECKMATE',['../constants_8h.html#ad20a7dc89555b944311c6022b0cd4d7b',1,'constants.h']]],
  ['flag_5fno_5fevent',['FLAG_NO_EVENT',['../constants_8h.html#a23cad842a4b7528f73baab46ea4dac0d',1,'constants.h']]],
  ['flag_5fstalemate',['FLAG_STALEMATE',['../constants_8h.html#a7d9304b26d03fc0b654226af48eb5b6b',1,'constants.h']]],
  ['flag_5fsurrender',['FLAG_SURRENDER',['../constants_8h.html#a4390fc1f6bc359e6bb40d66973482d8a',1,'constants.h']]]
];
