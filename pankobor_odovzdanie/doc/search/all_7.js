var searchData=
[
  ['invalid_5fargument_5fcartificial',['INVALID_ARGUMENT_CARTIFICIAL',['../constants_8h.html#a56d464efaeab29ea0ba9f216a37ea284',1,'constants.h']]],
  ['invalid_5fargument_5ftcoord',['INVALID_ARGUMENT_TCOORD',['../constants_8h.html#a59cdce9af94fcae0f97a5645f46821eb',1,'constants.h']]],
  ['ismoved',['isMoved',['../classCChessman.html#a5172bdc3caa8b2c358c89702ed48e9e2',1,'CChessman']]],
  ['isrunning',['isRunning',['../classCGame.html#a1f3ded29f7779ea87a86fe90a4a703e9',1,'CGame']]],
  ['issaved',['isSaved',['../classCGame.html#a9229dcc09040d26580727685205ccee0',1,'CGame']]],
  ['iswhite',['isWhite',['../classCChessman.html#a13a7856cae03212514fcd05101c2ca7e',1,'CChessman']]]
];
