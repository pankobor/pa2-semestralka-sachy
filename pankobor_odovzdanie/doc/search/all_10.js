var searchData=
[
  ['save',['save',['../classCArtificial.html#a9f8214f6b3036d10c73e77b877ede2f0',1,'CArtificial::save()'],['../classCBoard.html#a281f20dcd45a08ffa1fc34ab8d7146af',1,'CBoard::save()'],['../classCGame.html#af89513cd8d603bc01f84d4a7c4d31dbd',1,'CGame::save()'],['../classCPerson.html#a1c91647de147061837545f5c6e95a8aa',1,'CPerson::save()'],['../classCPlayer.html#a24876570a7a3d7bae5c202ce629cb9f9',1,'CPlayer::save()']]],
  ['savecommand',['saveCommand',['../commands_8cpp.html#af0b2d2d9de8a373ca88008b63501f024',1,'saveCommand():&#160;commands.cpp'],['../commands_8h.html#af0b2d2d9de8a373ca88008b63501f024',1,'saveCommand():&#160;commands.cpp']]],
  ['setmoved',['setMoved',['../classCChessman.html#ab730d0433ac8fa9e6b64033cb779b8da',1,'CChessman']]],
  ['setpawnsmoved',['setPawnsMoved',['../classCBoard.html#adfa4efd5440fa5a13a5edffbe54676a7',1,'CBoard']]],
  ['setrunning',['setRunning',['../classCGame.html#a2f40d014fd5348466786b9f202a6462f',1,'CGame']]],
  ['startnew',['startNew',['../classCGame.html#ae5b9689d49e262609ec20d2ff4881378',1,'CGame']]],
  ['surrendercommand',['surrenderCommand',['../commands_8cpp.html#a2dec3a76ed79a2421dd4bf7ec6523997',1,'surrenderCommand():&#160;commands.cpp'],['../commands_8h.html#a2dec3a76ed79a2421dd4bf7ec6523997',1,'surrenderCommand():&#160;commands.cpp']]]
];
