/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 6. 5. 2020
 */

#include "commands.h"

using namespace std;

CCommand newCommand() {
    return CCommand{
            HELP_NEW,
            [](const CInterface &interface, CGame &game, string &) {
                char decision = DECISION_YES;
                if (game.isRunning() && !game.isSaved()) {
                    decision = interface.promptWithOptions(CHOICE_INTERRUPT_GAME, CHOICES_INTERRUPT_GAME);
                }
                if (decision == DECISION_YES) {
                    unique_ptr<CPlayer> p1;
                    unique_ptr<CPlayer> p2;
                    char mode = interface.promptWithOptions(CHOICE_GAMEMODE, CHOICES_GAMEMODE);
                    //making dynamic binding to CPerson and CArtificial
                    if (mode == GAMEMODE_MULTIPLAYER) {
                        p1 = make_unique<CPerson>();
                        p2 = make_unique<CPerson>();
                    } else {
                        char difficulty = interface.promptWithOptions(CHOICE_DIFFICULTY, CHOICES_DIFFICULTY);
                        if (interface.promptWithOptions(CHOICE_COLOR, CHOICES_COLOR) == COLOR_WHITE) {
                            p1 = make_unique<CPerson>();
                            p2 = make_unique<CArtificial>(difficulty, false, false);
                        } else {
                            p1 = make_unique<CArtificial>(difficulty, true, false);
                            p2 = make_unique<CPerson>();
                        }
                    }
                    game.startNew(p1, p2);
                    interface.printBoard(game);
                }
                return true;
            }
    };
}

CCommand loadCommand() {
    return CCommand{
            HELP_LOAD,
            [](const CInterface &interface, CGame &game, string &filename) {
                bool example = false;
                if (filename.length() > 9 && filename.substr(0, 9) == FILE_EXAMPLES) {
                    example = true;
                } else if (filename.empty()) {
                    interface.print(ERROR_INVALID_FILENAME);
                    return true;
                }
                //check for not allowed characters in filename
                for (size_t i = (example ? 9 : 0); i < filename.length(); i++) {
                    if (!isalnum(filename[i]) && filename[i] != '_') {
                        interface.print(ERROR_INVALID_FILENAME);
                        return true;
                    }
                }
                char decision = DECISION_YES;
                if (game.isRunning() && !game.isSaved()) {
                    decision = interface.promptWithOptions(CHOICE_INTERRUPT_GAME, CHOICES_INTERRUPT_GAME);
                }
                if (decision == DECISION_YES) {
                    if (!example) {
                        filename.insert(0, FILE_SAVES);
                    }
                    ifstream ifs(filename);
                    if (!ifs.is_open() || ifs.fail() || ifs.bad() || ifs.eof()) {
                        interface.print(ERROR_INVALID_FILENAME);
                        return true;
                    }
                    if (!game.load(ifs, example)) {
                        interface.print(ERROR_INVALID_LOADING);
                    } else {
                        interface.printBoard(game);
                    }
                    ifs.close();
                }
                return true;
            }
    };
}

CCommand saveCommand() {
    return CCommand{
            HELP_SAVE,
            [](const CInterface &interface, CGame &game, string &filename) {
                if (!game.isRunning()) {
                    interface.print(ERROR_ONLY_INGAME);
                    return true;
                } else {
                    if (filename.empty()) {
                        interface.print(ERROR_INVALID_FILENAME);
                        return true;
                    }
                    //check for not allowed characters in filename
                    for (char c : filename) {
                        if (!isalnum(c) && c != '_') {
                            interface.print(ERROR_INVALID_FILENAME);
                            return true;
                        }
                    }
                    filename.insert(0, FILE_SAVES);
                    ofstream ofs(filename);
                    if (!game.save(ofs)) {
                        ofs.close();
                        interface.print(ERROR_INVALID_FILE);
                    }
                    ofs.close();
                    if (ofs.is_open()) {
                        interface.print(ERROR_INVALID_FILE);
                    }
                    return true;
                }
            }
    };
}

CCommand moveCommand() {
    return CCommand{
            HELP_MOVE,
            [](const CInterface &interface, CGame &game, string &parameters) {
                if (!game.isRunning()) {
                    interface.print(ERROR_ONLY_INGAME);
                } else {
                    //check the minimal length, then check that every character is valid coordinate
                    if (parameters.length() < 4 || parameters[1] < ROW_MIN || parameters[1] > ROW_MAX ||
                        parameters[3] < ROW_MIN || parameters[3] > ROW_MAX || parameters[0] < COLUMN_MIN ||
                        parameters[0] > COLUMN_MAX || parameters[2] > COLUMN_MAX || parameters[2] < COLUMN_MIN) {
                        interface.print(ERROR_INVALID_MOVE_NOTATION);
                    } else {
                        //if there is 5th char, check that it is mark of queen/rook/bishop/knight
                        //if not then remove it, also remove all after it, and it will use default q
                        if (parameters.length() >= 5 && parameters[4] != MARK_QUEEN && parameters[4] != MARK_ROOK &&
                            parameters[4] != MARK_BISHOP && parameters[4] != MARK_KNIGHT) {
                            parameters.erase(4);
                        }
                        int ending;
                        if (game.commitMove(parameters, ending)) {
                            interface.printBoard(game);
                            if (ending != FLAG_NO_EVENT) {
                                interface.printEvent(ending, game);
                            }
                        } else {
                            interface.print(ERROR_INVALID_MOVE);
                        }
                    }
                }
                return true;
            }
    };
}

CCommand surrenderCommand() {
    return CCommand{
            HELP_SURRENDER,
            [](const CInterface &interface, CGame &game, string &) {
                if (!game.isRunning()) {
                    interface.print(ERROR_ONLY_INGAME);
                } else {
                    interface.printEvent(FLAG_SURRENDER, game);
                    game.setRunning(false);
                }
                return true;
            }
    };
}

CCommand helpCommand(const map<string, CCommand> &commands) {
    return CCommand{
            HELP_HELP,
            [&commands](const CInterface &interface, CGame &, string &parameters) {
                if (parameters == NOTATION) {
                    interface.print(NOTATION_FILENAME);
                    interface.print(NOTATION_MOVE);
                } else {
                    for (const auto &c : commands) {
                        interface.printHelp(c.first, c.second.getHelp());
                    }
                }
                return true;
            }
    };
}

CCommand quitCommand() {
    return CCommand{
            HELP_QUIT,
            [](const CInterface &, CGame &, string &) {
                return false;
            }
    };
}
