/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 3. 5. 2020
 */

#include "CGame.h"

using namespace std;

CGame::CGame() : m_Running(false), m_Saved(false), m_MoveCount(0), m_Player1(nullptr), m_Player2(nullptr) {}

CGame::CGame(const CGame &src) : m_Running(src.m_Running), m_Saved(src.m_Saved), m_MoveCount(src.m_MoveCount),
                                 m_Board(src.m_Board), m_Player1(src.m_Player1->clone()),
                                 m_Player2(src.m_Player2->clone()) {}

CGame &CGame::operator=(const CGame &src) {
    if (this == &src) {
        return *this;
    }
    m_Running = src.m_Running;
    m_Saved = src.m_Saved;
    m_MoveCount = src.m_MoveCount;
    m_Board = src.m_Board;
    m_Player1 = src.m_Player1->clone();
    m_Player2 = src.m_Player2->clone();
    return *this;
}

void CGame::startNew(const std::unique_ptr<CPlayer> &p1, const std::unique_ptr<CPlayer> &p2) {
    m_Running = true;
    m_Saved = false;
    m_MoveCount = 0;
    m_Player1 = p1->clone();
    m_Player2 = p2->clone();
    m_Board.reset();
}

bool CGame::save(ofstream &ofs) {
    if (m_Running && ofs.is_open() && !ofs.bad() && !ofs.fail() &&
        m_Player1->save(ofs) && m_Player2->save(ofs) && m_Board.save(ofs)) {
        ofs << m_MoveCount << '\n';
        if (ofs.is_open() && !ofs.bad() && !ofs.fail()) {
            m_Saved = true;
            return true;
        }
    }
    return false;
}

bool CGame::load(ifstream &ifs, bool example) {
    char p1, p2;
    ifs.get(p1);
    ifs.get(p2);
    if (!ifs.is_open() || ifs.bad() || ifs.fail() || ifs.eof()) {
        return false;
    }
    if ((p1 != '1' && p1 != '2' && p1 != '3' && p1 != 'p') || (p2 != '1' && p2 != '2' && p2 != '3' && p2 != 'p') ||
        ((p1 == '1' || p1 == '2' || p1 == '3') && (p2 == '1' || p2 == '2' || p2 == '3') && !example)) {
        return false;
    }
    //if it's example also test that p1 and p2 are '1' or '2' then mark it sa an example also for AI
    if (p1 == 'p') {
        m_Player1 = make_unique<CPerson>();
    } else {
        m_Player1 = make_unique<CArtificial>(p1, true, p1 != 'p' && p2 != 'p' && p1 != '3' && p2 != '3');
    }
    if (p2 == 'p') {
        m_Player2 = make_unique<CPerson>();
    } else {
        m_Player2 = make_unique<CArtificial>(p2, false, p1 != 'p' && p2 != 'p' && p1 != '3' && p2 != '3');
    }
    char delim;
    ifs.get(delim);
    if (!ifs.is_open() || ifs.fail() || ifs.bad() || ifs.eof() || delim != '/') {
        return false;
    }
    CBoard new_board;
    int move_count;
    if (new_board.load(ifs, move_count) && ifs.is_open() && !ifs.bad() && !ifs.fail() && !ifs.eof()) {
        m_Running = true;
        m_Saved = true;
        m_Board = new_board;
        m_MoveCount = move_count;
        return true;
    }
    return false;
}

bool CGame::commitMove(const string &parameters, int &ending) {
    TCoord c1{parameters[0], parameters[1]};
    TCoord c2{parameters[2], parameters[3]};
    char promoted_to = MARK_QUEEN;
    //if the promoted_to parameter weren't given, then it is implicitly queen
    if (parameters.length() >= 5) {
        promoted_to = parameters.at(4);
    }
    bool white = m_MoveCount % 2 == 0;
    if (m_Board.validateMove(c1, c2, promoted_to, white)) {
        ending = m_Board.testEnd(white);
        if (ending == FLAG_CHECKMATE || ending == FLAG_STALEMATE) {
            m_Running = false;
        } else {
            m_MoveCount++;
        }
        m_Saved = false;
        return true;
    }
    return false;
}

size_t CGame::getMoveCount() const {
    return m_MoveCount;
}

bool CGame::isRunning() const {
    return m_Running;
}

void CGame::setRunning(bool mRunning) {
    m_Running = mRunning;
}

bool CGame::isSaved() const {
    return m_Saved;
}

const CBoard &CGame::getBoard() const {
    return m_Board;
}

const unique_ptr<CPlayer> &CGame::getPlayer(bool white) const {
    return (white ? m_Player1 : m_Player2);
}