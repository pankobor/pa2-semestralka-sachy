/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 6. 5. 2020
 */

#pragma once

#include <map>
#include <string>
#include <fstream>
#include "CCommand.h"
#include "CPlayer.h"
#include "CPerson.h"
#include "CArtificial.h"
#include "CGame.h"
#include "constants.h"

CCommand newCommand();

CCommand loadCommand();

CCommand saveCommand();

CCommand moveCommand();

CCommand surrenderCommand();

CCommand helpCommand(const std::map<std::string, CCommand> &commands);

CCommand quitCommand();