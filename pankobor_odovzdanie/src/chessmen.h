/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 7. 5. 2020
 */

#pragma once

#include <string>
#include "CChessman.h"
#include "TCoord.h"
#include "constants.h"

CChessman empty(bool white = false);

CChessman pawn(bool white);

CChessman rook(bool white);

CChessman knight(bool white);

CChessman bishop(bool white);

CChessman queen(bool white);

CChessman king(bool white);
