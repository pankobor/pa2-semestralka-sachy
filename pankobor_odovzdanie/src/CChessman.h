/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 6. 5. 2020
 */

#pragma once

#include <string>
#include <functional>
#include <iostream>
#include <vector>
#include "TCoord.h"

class CChessman {
    char m_Mark;
    std::function<std::vector<TCoord>(const TCoord &from, const TCoord &to)> m_Path;
    bool m_White;
    bool m_Moved;
public:
    CChessman() = default;

    CChessman(bool mWhite, char mMark,
              std::function<std::vector<TCoord>(const TCoord &from, const TCoord &to)> mPath,
              bool mMoved = false) noexcept;

    /**
     * Calculates the path of chessman between 2 fields. Checks only that the move of chessman meets the rules of
     * move of the specific chessman type.
     * @param from starting point of chessman
     * @param to ending point of chessman
     * @return vector of coordinates that the chessman would walk through if his move is valid, otherwise empty vector
     */
    std::vector<TCoord> getPath(const TCoord &from, const TCoord &to) const;

    /** @return true if chessman is white */
    bool isWhite() const;

    /** @return chessmans mark */
    char getMark() const;

    /** @return true if chessman was moved */
    bool isMoved() const;

    /** @param mMoved value used to set m_Move */
    void setMoved(bool mMoved);

    /**
     * Prints the chessman in output stream with changing his mark to uppercase if chessman is white
     * @param out defines the output strem
     * @param chessman defines the printed chessman
     * @return output stream "out"
     */
    friend std::ostream &operator<<(std::ostream &out, const CChessman &chessman);

    /**
     * @param lhs left chessman
     * @param rhs right chessman
     * @return true if chessmen are identical
     */
    friend bool operator==(const CChessman &lhs, const CChessman &rhs);

    /**
     * @param lhs left chessman
     * @param rhs right chessman
     * @return true if chessmen are differrent
     */
    friend bool operator!=(const CChessman &lhs, const CChessman &rhs);
};


