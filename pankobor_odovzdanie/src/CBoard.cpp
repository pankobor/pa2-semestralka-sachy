/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 4. 5. 2020
 */

#include "CBoard.h"

using namespace std;

//public----------------------------------------------------------------------------------------------------------------
CBoard::CBoard() {
    for (char c : string{COLUMNS}) {
        for (char r : string{ROWS}) {
            m_Fields.emplace(TCoord{c, r}, empty());
        }
    }
}

void CBoard::reset() {
    for (auto &m_Field : m_Fields) {
        if (m_Field.first == TCoord{COLUMN_A, ROW_8} || m_Field.first == TCoord{COLUMN_H, ROW_8}) {
            m_Field.second = rook(false);
        } else if (m_Field.first == TCoord{COLUMN_A, ROW_1} || m_Field.first == TCoord{COLUMN_H, ROW_1}) {
            m_Field.second = rook(true);
        } else if (m_Field.first == TCoord{COLUMN_B, ROW_8} || m_Field.first == TCoord{COLUMN_G, ROW_8}) {
            m_Field.second = knight(false);
        } else if (m_Field.first == TCoord{COLUMN_B, ROW_1} || m_Field.first == TCoord{COLUMN_G, ROW_1}) {
            m_Field.second = knight(true);
        } else if (m_Field.first == TCoord{COLUMN_C, ROW_8} || m_Field.first == TCoord{COLUMN_F, ROW_8}) {
            m_Field.second = bishop(false);
        } else if (m_Field.first == TCoord{COLUMN_C, ROW_1} || m_Field.first == TCoord{COLUMN_F, ROW_1}) {
            m_Field.second = bishop(true);
        } else if (m_Field.first == TCoord{COLUMN_D, ROW_8}) {
            m_Field.second = queen(false);
        } else if (m_Field.first == TCoord{COLUMN_D, ROW_1}) {
            m_Field.second = queen(true);
        } else if (m_Field.first == TCoord{COLUMN_E, ROW_8}) {
            m_Field.second = king(false);
        } else if (m_Field.first == TCoord{COLUMN_E, ROW_1}) {
            m_Field.second = king(true);
        } else if (m_Field.first.row == ROW_7) {
            m_Field.second = pawn(false);
        } else if (m_Field.first.row == ROW_2) {
            m_Field.second = pawn(true);
        } else {
            m_Field.second = empty();
        }
    }
}

const map<TCoord, CChessman> &CBoard::getFields() const {
    return m_Fields;
}

void CBoard::print(std::ostream &out) const {
    out << "\n   " << COLUMN_A << "  " << COLUMN_B << "  " << COLUMN_C << "  " << COLUMN_D << "  " << COLUMN_E << "  "
        << COLUMN_F << "  " << COLUMN_G << "  " << COLUMN_H << endl;
    out << " +------------------------+" << endl;
    for (const auto &field : m_Fields) {
        if (field.first.column == COLUMN_A) {
            out << field.first.row << "| ";
        }
        out << field.second << " ";
        if (field.first.column == COLUMN_H) {
            out << "|" << field.first.row << endl;
        } else {
            out << " ";
        }
    }
    out << " +------------------------+" << endl;
    out << "   " << COLUMN_A << "  " << COLUMN_B << "  " << COLUMN_C << "  " << COLUMN_D << "  " << COLUMN_E << "  "
        << COLUMN_F << "  " << COLUMN_G << "  " << COLUMN_H << "\n" << endl;
}

bool CBoard::save(ofstream &ofs) const {
    ofs << DELIMITER;
    for (char r = ROW_8; r >= ROW_1; r--) {
        for (char c : string{COLUMNS}) {
            ofs << m_Fields.at(TCoord{c, r});
            if (m_Fields.at(TCoord{c, r}).getMark() != MARK_EMPTY) {
                ofs << (m_Fields.at(TCoord{c, r}).isMoved() ? '1' : '0');
            }
        }
        ofs << DELIMITER;
    }
    return ofs.is_open() && !ofs.bad() && !ofs.fail();
}

bool CBoard::load(ifstream &ifs, int &move_count) {
    int king_w = 0, queen_w = 0, rook_w = 0, bishop_w = 0, knight_w = 0, pawn_w = 0;
    int king_b = 0, queen_b = 0, rook_b = 0, bishop_b = 0, knight_b = 0, pawn_b = 0;
    for (char r = ROW_8; r >= ROW_1; r--) {
        for (char c : string{COLUMNS}) {
            char mark;
            char moved;
            ifs.get(mark);
            if (!ifs.is_open() || ifs.fail() || ifs.bad() || ifs.eof()) {
                return false;
            }
            if (mark == MARK_ROOK || mark == toupper(MARK_ROOK)) {
                m_Fields.at(TCoord{c, r}) = rook(mark == toupper(MARK_ROOK));
                mark == toupper(MARK_ROOK) ? rook_w++ : rook_b++;
            } else if (mark == MARK_KNIGHT || mark == toupper(MARK_KNIGHT)) {
                m_Fields.at(TCoord{c, r}) = knight(mark == toupper(MARK_KNIGHT));
                mark == toupper(MARK_KNIGHT) ? knight_w++ : knight_b++;
            } else if (mark == MARK_BISHOP || mark == toupper(MARK_BISHOP)) {
                m_Fields.at(TCoord{c, r}) = bishop(mark == toupper(MARK_BISHOP));
                mark == toupper(MARK_BISHOP) ? bishop_w++ : bishop_b++;
            } else if (mark == MARK_QUEEN || mark == toupper(MARK_QUEEN)) {
                m_Fields.at(TCoord{c, r}) = queen(mark == toupper(MARK_QUEEN));
                mark == toupper(MARK_QUEEN) ? queen_w++ : queen_b++;
            } else if (mark == MARK_KING || mark == toupper(MARK_KING)) {
                m_Fields.at(TCoord{c, r}) = king(mark == toupper(MARK_KING));
                mark == toupper(MARK_KING) ? king_w++ : king_b++;
            } else if (mark == MARK_PAWN || mark == toupper(MARK_PAWN)) {
                m_Fields.at(TCoord{c, r}) = pawn(mark == toupper(MARK_PAWN));
                mark == toupper(MARK_PAWN) ? pawn_w++ : pawn_b++;
            } else if (mark != MARK_EMPTY) {
                return false;
            }
            if (mark != MARK_EMPTY) {
                ifs.get(moved);
                if (moved != '0' && moved != '1') {
                    return false;
                }
                m_Fields.at(TCoord{c, r}).setMoved(moved == '1');
            }
        }
        char delim;
        ifs.get(delim);
        if (!ifs.is_open() || ifs.fail() || ifs.bad() || ifs.eof() || delim != DELIMITER) {
            return false;
        }
    }
    //testing basic counts of chessmen to prevent loading corrupted file
    if (king_b != 1 || king_w != 1 || (king_w + queen_w + bishop_w + knight_w + rook_w + pawn_w) > 16 ||
        (king_b + queen_b + bishop_b + knight_b + rook_b + pawn_b) > 16 || pawn_w > 8 || pawn_b > 8 ||
        (queen_w + bishop_w + knight_w + rook_w - 7) > (8 - pawn_w) ||
        (queen_b + bishop_b + knight_b + rook_b - 7) > (8 - pawn_b)) {
        return false;
    }
    ifs >> move_count;
    int test_white = testEnd(true);
    int test_black = testEnd(false);
    return !(move_count < 0 || test_white == FLAG_CHECKMATE || test_white == FLAG_STALEMATE ||
             (test_white == FLAG_CHECK && move_count % 2 == 0) ||
             test_black == FLAG_CHECKMATE || test_black == FLAG_STALEMATE ||
             (test_black == FLAG_CHECK && move_count % 2 != 0));
}

bool CBoard::validateMove(const TCoord &from, const TCoord &to, char promoted_to, bool white) {
    if (m_Fields.at(from).isWhite() != white || m_Fields.at(from).getMark() == MARK_EMPTY || from == to) {
        return false;
    }
    if (tryCastling(from, to) || tryPromotion(from, to, promoted_to) || tryEnPassant(from, to)) {
        setPawnsMoved();
        return true;
    } else {
        vector<TCoord> path = m_Fields.at(from).getPath(from, to);
        //for king test that the target field is not threatened (he can't get into check)
        if (!path.empty() && testPath(path) &&
            (m_Fields.at(from).getMark() != MARK_KING || testCheck(to, white) == RETURN_NO_CHECK)) {
            CChessman backup = m_Fields.at(to);
            m_Fields.at(to) = m_Fields.at(from);
            m_Fields.at(from) = empty();
            //also test if player exposed his king by moving badly with other chessman -> not allowed, move isn't valid
            if (testCheck(white)) {
                m_Fields.at(from) = m_Fields.at(to);
                m_Fields.at(to) = backup;
                return false;
            }
            m_Fields.at(from) = m_Fields.at(to);
            setPawnsMoved();
            m_Fields.at(to) = m_Fields.at(from);
            //if pawn moved forward by 2, delay setting him moved for 1 round so the en passant can be done in next round
            m_Fields.at(to).setMoved(m_Fields.at(from).getMark() != MARK_PAWN || abs(from.row - to.row) != 2);
            m_Fields.at(from) = empty();
            return true;
        }
    }
    return false;
}

void CBoard::undoMove(const TCoord &from, const TCoord &to, const map<TCoord, CChessman> &original_fields) {
    m_Fields.at(from) = original_fields.at(from);
    m_Fields.at(to) = original_fields.at(to);
    for (char c : string{COLUMNS}) {
        for (char r = ROW_4; r < ROW_6; r++) {
            if (m_Fields.at(TCoord{c, r}) != original_fields.at(TCoord{c, r})) {
                m_Fields.at(TCoord{c, r}) = original_fields.at(TCoord{c, r});
            }
        }
    }
}

bool CBoard::testCheck(bool white) const {
    for (const auto &field : m_Fields) {
        if (field.second.getMark() == MARK_KING && field.second.isWhite() == white) {
            return testCheck(field.first, white) != RETURN_NO_CHECK;
        }
    }
    return false;
}

int CBoard::testEnd(bool white) {
    bool check = testCheck(!white);
    if (check && testCheckmate(!white)) {
        return FLAG_CHECKMATE;
    } else if (!check && testStalemate(!white)) {
        return FLAG_STALEMATE;
    } else if (check) {
        return FLAG_CHECK;
    }
    return FLAG_NO_EVENT;
}

//private---------------------------------------------------------------------------------------------------------------
bool CBoard::tryCastling(const TCoord &from, const TCoord &to) {
    if (m_Fields.at(from).getMark() != MARK_KING) {
        return false;
    } else if (from == TCoord{COLUMN_E, ROW_1} && to == TCoord{COLUMN_G, ROW_1} &&
               !m_Fields.at(from).isMoved() && !m_Fields[TCoord{COLUMN_H, ROW_1}].isMoved() &&
               m_Fields[TCoord{COLUMN_F, ROW_1}].getMark() == MARK_EMPTY &&
               m_Fields[TCoord{COLUMN_G, ROW_1}].getMark() == MARK_EMPTY &&
               testCheck(TCoord{COLUMN_F, ROW_1}, true) == RETURN_NO_CHECK &&
               testCheck(TCoord{COLUMN_G, ROW_1}, true) == RETURN_NO_CHECK &&
               testCheck(TCoord{COLUMN_E, ROW_1}, true) == RETURN_NO_CHECK) {
        m_Fields.at(to) = m_Fields[from];
        m_Fields.at(to).setMoved(true);
        m_Fields[TCoord{COLUMN_F, ROW_1}] = m_Fields[TCoord{COLUMN_H, ROW_1}];
        m_Fields[TCoord{COLUMN_F, ROW_1}].setMoved(true);
        m_Fields.at(from) = empty();
        m_Fields[TCoord{COLUMN_H, ROW_1}] = empty();
    } else if (from == TCoord{COLUMN_E, ROW_1} && to == TCoord{COLUMN_B, ROW_1} &&
               !m_Fields.at(from).isMoved() && !m_Fields[TCoord{COLUMN_A, ROW_1}].isMoved() &&
               m_Fields[TCoord{COLUMN_B, ROW_1}].getMark() == MARK_EMPTY &&
               m_Fields[TCoord{COLUMN_C, ROW_1}].getMark() == MARK_EMPTY &&
               m_Fields[TCoord{COLUMN_D, ROW_1}].getMark() == MARK_EMPTY &&
               testCheck(TCoord{COLUMN_D, ROW_1}, true) == RETURN_NO_CHECK &&
               testCheck(TCoord{COLUMN_C, ROW_1}, true) == RETURN_NO_CHECK &&
               testCheck(TCoord{COLUMN_B, ROW_1}, true) == RETURN_NO_CHECK &&
               testCheck(TCoord{COLUMN_E, ROW_1}, true) == RETURN_NO_CHECK) {
        m_Fields.at(to) = m_Fields[from];
        m_Fields.at(to).setMoved(true);
        m_Fields[TCoord{COLUMN_C, ROW_1}] = m_Fields[TCoord{COLUMN_A, ROW_1}];
        m_Fields[TCoord{COLUMN_C, ROW_1}].setMoved(true);
        m_Fields.at(from) = empty();
        m_Fields[TCoord{COLUMN_A, ROW_1}] = empty();
    } else if (from == TCoord{COLUMN_E, ROW_8} && to == TCoord{COLUMN_G, ROW_8} &&
               !m_Fields.at(from).isMoved() && !m_Fields[TCoord{COLUMN_H, ROW_8}].isMoved() &&
               m_Fields[TCoord{COLUMN_F, ROW_8}].getMark() == MARK_EMPTY &&
               m_Fields[TCoord{COLUMN_G, ROW_8}].getMark() == MARK_EMPTY &&
               testCheck(TCoord{COLUMN_F, ROW_8}, false) == RETURN_NO_CHECK &&
               testCheck(TCoord{COLUMN_G, ROW_8}, false) == RETURN_NO_CHECK &&
               testCheck(TCoord{COLUMN_E, ROW_8}, false) == RETURN_NO_CHECK) {
        m_Fields.at(to) = m_Fields[from];
        m_Fields.at(to).setMoved(true);
        m_Fields[TCoord{COLUMN_F, ROW_8}] = m_Fields[TCoord{COLUMN_H, ROW_8}];
        m_Fields[TCoord{COLUMN_F, ROW_8}].setMoved(true);
        m_Fields.at(from) = empty();
        m_Fields[TCoord{COLUMN_H, ROW_8}] = empty();
    } else if (from == TCoord{COLUMN_E, ROW_8} && to == TCoord{COLUMN_B, ROW_8} &&
               !m_Fields.at(from).isMoved() && !m_Fields[TCoord{COLUMN_A, ROW_8}].isMoved() &&
               m_Fields[TCoord{COLUMN_B, ROW_8}].getMark() == MARK_EMPTY &&
               m_Fields[TCoord{COLUMN_C, ROW_8}].getMark() == MARK_EMPTY &&
               m_Fields[TCoord{COLUMN_D, ROW_8}].getMark() == MARK_EMPTY &&
               testCheck(TCoord{COLUMN_D, ROW_8}, false) == RETURN_NO_CHECK &&
               testCheck(TCoord{COLUMN_C, ROW_8}, false) == RETURN_NO_CHECK &&
               testCheck(TCoord{COLUMN_B, ROW_8}, false) == RETURN_NO_CHECK &&
               testCheck(TCoord{COLUMN_E, ROW_8}, false) == RETURN_NO_CHECK) {
        m_Fields.at(to) = m_Fields[from];
        m_Fields.at(to).setMoved(true);
        m_Fields[TCoord{COLUMN_C, ROW_8}] = m_Fields[TCoord{COLUMN_A, ROW_8}];
        m_Fields[TCoord{COLUMN_C, ROW_8}].setMoved(true);
        m_Fields.at(from) = empty();
        m_Fields[TCoord{COLUMN_A, ROW_8}] = empty();
    } else {
        return false;
    }
    return true;
}

bool CBoard::tryPromotion(const TCoord &from, const TCoord &to, const char &promoted_to) {
    if (m_Fields.at(from).getMark() == MARK_PAWN && (to.row == ROW_1 || to.row == ROW_8) &&
        !m_Fields.at(from).getPath(from, to).empty() &&
        testPath(m_Fields.at(from).getPath(from, to))) {
        if (promoted_to == MARK_ROOK) {
            m_Fields.at(to) = rook(m_Fields.at(from).isWhite());
        } else if (promoted_to == MARK_BISHOP) {
            m_Fields.at(to) = bishop(m_Fields.at(from).isWhite());
        } else if (promoted_to == MARK_KNIGHT) {
            m_Fields.at(to) = knight(m_Fields.at(from).isWhite());
        } else {    //queen is the default choice
            m_Fields.at(to) = queen(m_Fields.at(from).isWhite());
        }
        m_Fields.at(to).setMoved(true);
        m_Fields.at(from) = empty();
        return true;
    }
    return false;
}

bool CBoard::tryEnPassant(const TCoord &from, const TCoord &to) {
    if (m_Fields.at(from).getMark() == MARK_PAWN && from.column != to.column &&
        !m_Fields.at(from).getPath(from, to).empty() && !m_Fields.at(TCoord{to.column, from.row}).isMoved() &&
        m_Fields.at(TCoord{to.column, from.row}).isWhite() != m_Fields.at(from).isWhite() &&
        m_Fields.at(to).getMark() == MARK_EMPTY && m_Fields.at(TCoord{to.column, from.row}).getMark() == MARK_PAWN) {
        m_Fields.at(to) = m_Fields.at(from);
        m_Fields.at(to).setMoved(true);
        m_Fields.at(from) = empty();
        m_Fields.at(TCoord{to.column, from.row}) = empty();
        return true;
    }
    return false;
}

TCoord CBoard::testCheck(const TCoord &target_field, bool white) const {
    for (const auto &field : m_Fields) {
        //for enemy test if the enemy can attack the target_field
        if (field.second.getMark() != MARK_EMPTY && field.second.isWhite() != white && field.first != target_field) {
            if (testPath(field.second.getPath(field.first, target_field))) {
                return field.first;
            }
        }
    }
    return RETURN_NO_CHECK;
}

bool CBoard::testPath(const vector<TCoord> &path) const {
    if (path.size() < 2) {
        return false;
    }
    //test that path is free
    for (size_t i = 1; i < path.size() - 1; i++) {
        if (m_Fields.at(path[i]).getMark() != MARK_EMPTY) {
            return false;
        }
    }
    //test if end is empty or enemy
    //for pawn test if target field is either empty (when moving straight) or enemy (when moving sideways)
    return !((m_Fields.at(path[path.size() - 1]).getMark() != MARK_EMPTY &&
              m_Fields.at(path[0]).isWhite() == m_Fields.at(path[path.size() - 1]).isWhite()) ||
             (m_Fields.at(path[0]).getMark() == MARK_PAWN &&
              ((m_Fields.at(path[path.size() - 1]).getMark() != MARK_EMPTY &&
                path[0].column == path[path.size() - 1].column) ||
               ((m_Fields.at(path[path.size() - 1]).getMark() == MARK_EMPTY &&
                 path[0].column != path[path.size() - 1].column)))));
}

bool CBoard::testCheckmate(bool white) {
    //firstly test if the king can make a move that will avoid check on him
    TCoord king_coord = RETURN_NO_CHECK;
    for (const auto &field : m_Fields) {
        if (field.second.getMark() == MARK_KING && field.second.isWhite() == white) {
            king_coord = field.first;
        }
    }
    for (int i = -1; i < 2; i++) {
        for (int j = -1; j < 2; j++) {
            if ((i != 0 || j != 0) && king_coord.row + i >= ROW_MIN && king_coord.row + i <= ROW_MAX &&
                king_coord.column + j >= COLUMN_MIN && king_coord.column + j <= COLUMN_MAX) {
                char r = king_coord.row;
                char c = king_coord.column;
                if (i == -1) {
                    r--;
                } else if (i == 1) {
                    r++;
                }
                if (j == -1) {
                    c--;
                } else if (j == 1) {
                    c++;
                }
                TCoord target_coord(c, r);
                if (testPath(m_Fields.at(king_coord).getPath(king_coord, target_coord))) {
                    CChessman backup = m_Fields.at(target_coord);
                    m_Fields.at(target_coord) = m_Fields.at(king_coord);
                    m_Fields.at(king_coord) = empty();
                    if (!testCheck(white)) {
                        m_Fields.at(king_coord) = m_Fields.at(target_coord);
                        m_Fields.at(target_coord) = backup;
                        return false;
                    }
                    m_Fields.at(king_coord) = m_Fields.at(target_coord);
                    m_Fields.at(target_coord) = backup;
                }
            }
        }
    }
    //if not, test if it's possible to remove threatening enemy chessman
    TCoord enemy_coord = testCheck(king_coord, white);
    CChessman enemy_backup = m_Fields.at(enemy_coord);
    for (const auto &field : m_Fields) {
        if (field.second.getMark() != MARK_EMPTY && field.second.isWhite() == white &&
            testPath(field.second.getPath(field.first, enemy_coord))) {
            CChessman my_backup = m_Fields.at(field.first);
            m_Fields.at(enemy_coord) = field.second;
            m_Fields.at(field.first) = empty();
            if (!testCheck(white)) {
                m_Fields.at(enemy_coord) = enemy_backup;
                m_Fields.at(field.first) = my_backup;
                return false;
            }
            m_Fields.at(enemy_coord) = enemy_backup;
            m_Fields.at(field.first) = my_backup;
        }
    }
    //if removing isn't possible and the threatening chessman is knight, then it's checkmate, because knight jump over fields
    if (m_Fields.at(enemy_coord).getMark() == MARK_KNIGHT) {
        return true;
    }
    //if removing isn't possible and threat is not a knight, try to get in the path of the threatening chessman
    vector<TCoord> path = m_Fields.at(enemy_coord).getPath(enemy_coord, king_coord);
    for (const auto &field : m_Fields) {
        if (field.second.getMark() != MARK_EMPTY && field.second.isWhite() == white) {
            for (size_t i = 1; i < path.size() - 1; i++) {
                if (testPath(field.second.getPath(field.first, path.at(i)))) {
                    CChessman my_backup = m_Fields.at(field.first);
                    CChessman path_backup = m_Fields.at(path.at(i));
                    m_Fields.at(path.at(i)) = field.second;
                    m_Fields.at(field.first) = empty();
                    if (!testCheck(white)) {
                        m_Fields.at(path.at(i)) = path_backup;
                        m_Fields.at(field.first) = my_backup;
                        return false;
                    }
                    m_Fields.at(path.at(i)) = path_backup;
                    m_Fields.at(field.first) = my_backup;
                }
            }
        }
    }
    return true;
}

bool CBoard::testStalemate(bool white) const {
    //for each chessman test if it can move somewhere
    for (const auto &field : m_Fields) {
        if (field.second.getMark() != MARK_EMPTY && field.second.isWhite() == white) {
            for (const auto &potential_target : m_Fields) {
                if (potential_target.first != field.first &&
                    testPath(field.second.getPath(field.first, potential_target.first)) &&
                    (field.second.getMark() != MARK_KING ||
                     testCheck(potential_target.first, white) == RETURN_NO_CHECK)) {
                    //if at least one chessman can move, then it's not stalemate
                    return false;
                }
            }
        }
    }
    return true;
}

void CBoard::setPawnsMoved() {
    for (char c : string{COLUMNS}) {
        for (char r = ROW_4; r < ROW_6; r++) {
            m_Fields.at(TCoord{c, r}).setMoved(true);
        }
    }
}