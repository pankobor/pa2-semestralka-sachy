/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 9. 5. 2020
 */

#include "CArtificial.h"

using namespace std;

//public----------------------------------------------------------------------------------------------------------------
CArtificial::CArtificial(char diff, bool white, bool example) : m_Difficulty(diff), m_White(white), m_Example(example) {
    if (diff != DIFFICULTY_LOW && diff != DIFFICULTY_MEDIUM && diff != DIFFICULTY_HIGH) {
        throw invalid_argument(INVALID_ARGUMENT_CARTIFICIAL);
    }
}

bool CArtificial::save(ofstream &ofs) const {
    ofs << m_Difficulty;
    return ofs.is_open() && !ofs.bad() && !ofs.fail();
}

void CArtificial::endMessage(ostream &out, bool white) const {
    if (white == m_White && !m_Example) {
        out << EVENT_PERSON_LOOSE << endl;
    } else if (!m_Example) {
        out << EVENT_PERSON_WIN << endl;
    }
}

unique_ptr<CPlayer> CArtificial::clone() const {
    return make_unique<CArtificial>(*this);
}

string CArtificial::requestMove(istream &in, ostream &out, const CBoard &board, string &params) {
    if (m_Difficulty == DIFFICULTY_LOW) {
        params = calculateMoveEasy(board);
    } else if (m_Difficulty == DIFFICULTY_MEDIUM) {
        params = calculateMoveMedium(board);
    } else {
        out << TEXT_AI_THINKING << endl;
        params = calculateMoveHard(board);
        out << TEXT_AI_MOVE;
    }
    out << params << endl;
    //if it's not example or AI is level 3, put a '\n' to istream
    //that will cause auto refresh of interface after the AI makes a move
    if (!m_Example || m_Difficulty == 3) {
        in.putback('\n');
    }
    return COMMAND_MOVE;
}

//private---------------------------------------------------------------------------------------------------------------
string CArtificial::calculateMoveEasy(const CBoard &original_board) const {
    CBoard board = original_board;
    vector<TCoord> my_chessmen;
    vector<TCoord> possible_targets;
    for (const auto &field : original_board.getFields()) {
        if (field.second.getMark() != MARK_EMPTY && field.second.isWhite() == m_White) {
            my_chessmen.push_back(field.first);
        } else {
            possible_targets.push_back(field.first);
        }
    }
    vector<pair<TCoord, TCoord>> possible_checks;
    vector<pair<TCoord, TCoord>> possible_moves;
    for (const TCoord &chessman_coord : my_chessmen) {
        for (const TCoord &target_coord : possible_targets) {
            if (board.validateMove(chessman_coord, target_coord, MARK_QUEEN, m_White)) {
                int ending = board.testEnd(m_White);
                if (ending == FLAG_CHECKMATE) {
                    //if it's checkmate, it's immediately returned, because better move doesn't exists
                    return chessman_coord.toString() + target_coord.toString();
                } else if (ending == FLAG_CHECK) {  //dividing check and other moves
                    possible_checks.emplace_back(chessman_coord, target_coord);
                } else {
                    possible_moves.emplace_back(chessman_coord, target_coord);
                }
            }
            board.undoMove(chessman_coord, target_coord, original_board.getFields());
        }
    }
    /**generating random numbers source: https://en.cppreference.com/w/cpp/numeric/random/uniform_int_distribution */
    random_device rd;
    mt19937 gen(rd());
    //preferring picking a random check move
    if (!possible_checks.empty()) {
        uniform_int_distribution<size_t> index_checks(0, possible_checks.size() - 1);
        size_t i = index_checks(gen);
        return possible_checks[i].first.toString() + possible_checks[i].second.toString();
    }
    uniform_int_distribution<size_t> index_moves(0, possible_moves.size() - 1);
    size_t i = index_moves(gen);
    return possible_moves[i].first.toString() + possible_moves[i].second.toString();
}

std::string CArtificial::calculateMoveMedium(const CBoard &original_board) const {
    int value, best_value = numeric_limits<int>::min();
    int i = 0, best_index = 0;
    CBoard board = original_board;
    vector<TCoord> my_chessmen;
    vector<TCoord> possible_targets;
    for (const auto &field : original_board.getFields()) {
        if (field.second.getMark() != MARK_EMPTY && field.second.isWhite() == m_White) {
            my_chessmen.push_back(field.first);
        } else {
            possible_targets.push_back(field.first);
        }
    }
    vector<pair<TCoord, TCoord>> possible_moves;
    for (const TCoord &chessman_coord : my_chessmen) {
        for (const TCoord &target_coord : possible_targets) {
            if (board.validateMove(chessman_coord, target_coord, MARK_QUEEN, m_White)) {
                int ending = board.testEnd(m_White);
                if (ending == FLAG_CHECKMATE) {
                    return chessman_coord.toString() + target_coord.toString();
                }
                possible_moves.emplace_back(chessman_coord, target_coord);
                //negative value because enemy will be the side on the move, so the position value will be calculated for him
                value = CStaticBoardEvaluator::calculatePositionValue(board.getFields(), !m_White);
                value *= -1;
                if (ending == FLAG_CHECK) {
                    value += BONUS_CHECK;
                }
                if (value > best_value) {
                    best_value = value;
                    best_index = i;
                }
                i++;
            }
            board.undoMove(chessman_coord, target_coord, original_board.getFields());
        }
    }
    return possible_moves[best_index].first.toString() + possible_moves[best_index].second.toString();
}

std::string CArtificial::calculateMoveHard(const CBoard &original_board) const {
    int value, best_value = PENALTY_CHECKMATE;
    int i = 0, best_index = 0;
    CBoard board = original_board;
    vector<TCoord> my_chessmen;
    vector<TCoord> possible_targets;
    for (const auto &field : original_board.getFields()) {
        if (field.second.getMark() != MARK_EMPTY && field.second.isWhite() == m_White) {
            my_chessmen.push_back(field.first);
        } else {
            possible_targets.push_back(field.first);
        }
    }
    vector<pair<TCoord, TCoord>> possible_moves;
    for (const TCoord &chessman_coord : my_chessmen) {
        for (const TCoord &target_coord : possible_targets) {
            if (board.validateMove(chessman_coord, target_coord, MARK_QUEEN, m_White)) {
                if (board.testEnd(m_White) == FLAG_CHECKMATE) {
                    return chessman_coord.toString() + target_coord.toString();
                }
                possible_moves.emplace_back(chessman_coord, target_coord);
                //evaluating move with negamax algorithm instead of static evaluation function
                value = negamax(board, -1 * best_value, NEGAMAX_DEPTH, !m_White);
                value *= -1;
                if (value > best_value) {
                    best_value = value;
                    best_index = i;
                }
                i++;
            }
            board.undoMove(chessman_coord, target_coord, original_board.getFields());
        }
    }
    return possible_moves[best_index].first.toString() + possible_moves[best_index].second.toString();
}


int CArtificial::negamax(const CBoard &original_board, const int alpha, int depth, bool white) const {
    if (depth <= 0) {
        //evaluating terminal nodes with static evaluation function
        return CStaticBoardEvaluator::calculatePositionValue(original_board.getFields(), white);
    }
    int value, best_value = PENALTY_CHECKMATE;
    int move_count = 0;
    CBoard board = original_board;
    vector<TCoord> my_chessmen;
    vector<TCoord> possible_targets;
    for (const auto &field : original_board.getFields()) {
        if (field.second.getMark() != MARK_EMPTY && field.second.isWhite() == white) {
            my_chessmen.push_back(field.first);
        } else {
            possible_targets.push_back(field.first);
        }
    }
    for (const TCoord &chessman_coord : my_chessmen) {
        for (const TCoord &target_coord : possible_targets) {
            if (board.validateMove(chessman_coord, target_coord, MARK_QUEEN, white)) {
                value = negamax(board, -1 * best_value, depth - 1, !white);
                value *= -1;
                if (value > best_value) {
                    best_value = value;
                }
                //cutting off other branches if the best_value is greater than alpha that means, the move in this node
                // will be surely at least of best_value, therefore if it's more than alpha it surely won't be chosen
                if (best_value > alpha) {
                    return best_value;
                }
                move_count++;
            }
            board.undoMove(chessman_coord, target_coord, original_board.getFields());
        }
    }
    //testing end only if no move is possible, which makes 2 options: checkmate (with check) or stalemate (no check)
    if (move_count == 0) {
        if (original_board.testCheck(white)) {
            best_value = PENALTY_CHECKMATE;
        } else {
            best_value = BONUS_STALEMATE;
        }
    }
    return best_value;
}



