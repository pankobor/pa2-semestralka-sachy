/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 9. 5. 2020
 */

#pragma once

#include <string>
#include <iostream>
#include <random>
#include <map>
#include "CPlayer.h"
#include "CStaticBoardEvaluator.h"

class CArtificial : public CPlayer {
    char m_Difficulty;
    bool m_White;
    bool m_Example;

    /**
     * @param original_board used to generate possible moves
     * @return random move but preferring the checkmate and check
     */
    std::string calculateMoveEasy(const CBoard &original_board) const;

    /**
     * Calculates best move by trying every possible move and then evaluating it with CStaticBoardEvaluator.
     * @param original_board used for evaluation of position
     * @return best calculated move
     */
    std::string calculateMoveMedium(const CBoard &original_board) const;

    /**
     * Calculates best move by evaluating every possible move with negamax algorithm.
     * @param original_board used for trying few moves forward and then evaluating position
     * @return best calculated move
     */
    std::string calculateMoveHard(const CBoard &original_board) const;

    /**
     * Recursive negamax algorithm to evaluate every possible move with alpha-beta pruning eliminating moves that surely
     * won't be chosen increasing efficiency. Every terminal node is evaluated with CStaticBoardEvaluator.
     * Every non-terminal node value is determined by assuming:
     *      - The value of a position is the value of best move that can be made from it.
     *      - The value of move is the negative of the value of the position that results from it.
     * In other words, this algorithm chooses the move that will limit enemies moves pool to worst possible.
     * @param original_board for making move and evaluating it
     * @param alpha for alpha-beta pruning
     * @param depth for recursive calling, and static evaluation when reaches 0
     * @param white indicates player
     * @return value of best move in current node of negamax recursive tree
     */
    int negamax(const CBoard &original_board, int alpha, int depth, bool white) const;

public:
    /** @throws std::invalid_argument exception if the difficulty is not valid */
    CArtificial(char diff, bool white, bool example);

    /**
     * Saves the AI m_Difficulty to savefile
     * @param ofs defines the output filestream of savefile
     * @return true if saving was successful and if the ifs is OK
     */
    bool save(std::ofstream &ofs) const override;

    /** Prints a "You win"/"You lost" announcement
     * @param out output stream where the announcement will be printed
     * @param white defines which of the announcement will be printed
     */
    void endMessage(std::ostream &out, bool white) const override;

    /** @return unique_ptr to AI */
    std::unique_ptr<CPlayer> clone() const override;

    /**
     * Calculates a move depending on m_Difficulty.
     * @param in is used to set if the interface will be auto updated after the AI makes a move
     * @param out is used to display the move parameters so the enemy player can see it
     * @param board for calculating the move, bacuse the AI can't see the board in terminal as person
     * @param params output parameter for coordinates of calculated move.
     * @return string "move"
     */
    std::string requestMove(std::istream &in, std::ostream &out, const CBoard &board, std::string &params) override;
};