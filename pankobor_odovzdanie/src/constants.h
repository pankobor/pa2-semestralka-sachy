/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 6. 5. 2020
 */

#pragma once
#include <map>
#include <string>

const char *const HELP_NEW = "Start new game.";
const char *const HELP_LOAD = "Load saved game from <filename>. If you want to load an example, use \"load examples\\<filename>\".";
const char *const HELP_SAVE = "Save the current game to <filename>.";
const char *const HELP_MOVE = "Moves chessman to new square. Uses a classic chess pure coordinate notation.";
const char *const HELP_SURRENDER = "Surrender and stops currently running game.";
const char *const HELP_HELP = "Show list of commands. If you want to see the filename or move descriptor notation, use \"help notation\".";
const char *const HELP_QUIT = "Stops running program.";

const char *const COMMAND_NEW = "new";
const char *const COMMAND_LOAD = "load";
const char *const COMMAND_SAVE = "save";
const char *const COMMAND_MOVE = "move";
const char *const COMMAND_SURRENDER = "surrender";
const char *const COMMAND_HELP = "help";
const char *const COMMAND_QUIT = "quit";

const char *const NOTATION = "notation";
const char *const NOTATION_MOVE = "Move descriptor notation:\n"
                                  "\t<move descriptor> ::= <from square><to square>[promoted to]\n"
                                  "\t<square>          ::= <file letter><rank number>\n"
                                  "\t<file letter>     ::= 'a'|'b'|'c'|'d'|'e'|'f'|'g'|'h'\n"
                                  "\t<rank number>     ::= '1'|'2'|'3'|'4'|'5'|'6'|'7'|'8'\n"
                                  "\t<promoted to>     ::= 'q'|'r'|'b'|'n'";
const char *const NOTATION_FILENAME = "Filename notation: can only contain alphanumeric characters or \'_\'.";

const char *const TEXT_INTRO = "\n\nWelcome to classic Command Line Chess!\n"
                               "Use \"help\" to see all commands.";
const char *const TEXT_AI_THINKING = "Thinking...";
const char *const TEXT_AI_MOVE = "My move is ";

const char *const ERROR_INVALID_INPUT = "Unvalidated input.";
const char *const ERROR_ONLY_INGAME = "This command can't be used while game is not running.";
const char *const ERROR_INVALID_MOVE = "Invalid move.";
const char *const ERROR_INVALID_MOVE_NOTATION = "Invalid move parameter notation.";
const char *const ERROR_INVALID_FILE = "An error occured while accessing file.";
const char *const ERROR_INVALID_LOADING = "The save-file is corrupted. Game cannot be loaded.";
const char *const ERROR_INVALID_FILENAME = "Invalid filename.";
const char *const ERROR_INVALID_CHOICE = "Invalid choice. Please use one of the listed options.";
const char *const ERROR_INVALID_COMMAND = "Invalid command. Use \"help\" for list of supported commands.";

const char *const INVALID_ARGUMENT_CARTIFICIAL = "Invalid AI difficulty.";
const char *const INVALID_ARGUMENT_TCOORD = "Invalid coordinates.";

const char *const FILE_SAVES = "saves/";
const char *const FILE_EXAMPLES = "examples/";

const char *const EVENT_CHECK = "Check! ";
const char *const EVENT_CHECKMATE = "Checkmate!";
const char *const EVENT_STALEMATE = "Stalemate!\nWhite 1/2-1/2 Black";
const char *const EVENT_WHITE_WIN = "White 1-0 Black";
const char *const EVENT_BLACK_WIN = "White 0-1 Black";
const char *const EVENT_PERSON_WIN = "You win!";
const char *const EVENT_PERSON_LOOSE = "You lost!";

const char *const CHOICE_INTERRUPT_GAME = "The game is running, if you quit now all progress will be lost!\n"
                                          "Do you really want to quit this game? [y/n]: ";
const char *const CHOICE_GAMEMODE = "Choose game mode (s..singleplayer,m..multiplayer) [s/m]: ";
const char *const CHOICE_DIFFICULTY = "Select the difficulty of AI (1..easy,2..medium,3..hard) [1/2/3]: ";
const char *const CHOICE_COLOR = "Select your color [w/b]: ";

const char *const CHOICES_INTERRUPT_GAME = "yn";
const char *const CHOICES_GAMEMODE = "sm";
const char *const CHOICES_DIFFICULTY = "123";
const char *const CHOICES_COLOR = "wb";

const char DECISION_YES = CHOICES_INTERRUPT_GAME[0];
const char DECISION_NO = CHOICES_INTERRUPT_GAME[1];
const char GAMEMODE_SINGLEPLAYER = CHOICES_GAMEMODE[0];
const char GAMEMODE_MULTIPLAYER = CHOICES_GAMEMODE[1];
const char DIFFICULTY_LOW = CHOICES_DIFFICULTY[0];
const char DIFFICULTY_MEDIUM = CHOICES_DIFFICULTY[1];
const char DIFFICULTY_HIGH = CHOICES_DIFFICULTY[2];
const char COLOR_WHITE = CHOICES_COLOR[0];
const char COLOR_BLACK = CHOICES_COLOR[1];

const char *const RETURN_NO_CHECK = "00";

const char MARK_KING = 'k';
const char MARK_QUEEN = 'q';
const char MARK_ROOK = 'r';
const char MARK_BISHOP = 'b';
const char MARK_KNIGHT = 'n';
const char MARK_PAWN = 'p';
const char MARK_EMPTY = '.';
const char MARK_PLAYER = 'p';
const char DELIMITER = '/';
//columns and rows has to be ASCII characters, that are next to each other in ASCII table and in order as in ASCII
const char *const COLUMNS = "abcdefgh";
const char *const ROWS = "12345678"; //for example "()*+,-./"

const char COLUMN_MIN = COLUMNS[0];
const char COLUMN_MAX = COLUMNS[7];
const char ROW_MIN = ROWS[0];
const char ROW_MAX = ROWS[7];
const char COLUMN_A = COLUMNS[0];
const char COLUMN_B = COLUMNS[1];
const char COLUMN_C = COLUMNS[2];
const char COLUMN_D = COLUMNS[3];
const char COLUMN_E = COLUMNS[4];
const char COLUMN_F = COLUMNS[5];
const char COLUMN_G = COLUMNS[6];
const char COLUMN_H = COLUMNS[7];
const char ROW_1 = ROWS[0];
const char ROW_2 = ROWS[1];
const char ROW_3 = ROWS[2];
const char ROW_4 = ROWS[3];
const char ROW_5 = ROWS[4];
const char ROW_6 = ROWS[5];
const char ROW_7 = ROWS[6];
const char ROW_8 = ROWS[7];

const int NEGAMAX_DEPTH = 3; //suggested max 4, otherwise very very slow

const int FLAG_NO_EVENT = 0;
const int FLAG_CHECK = 1;
const int FLAG_CHECKMATE = 2;
const int FLAG_STALEMATE = 3;
const int FLAG_SURRENDER = 4;

const int PHASE_EARLY = 5;
const int PHASE_MIDDLE = 6;
const int PHASE_END = 7;

const int QUADRANT_FIRST = 8;
const int QUADRANT_SECOND = 9;
const int QUADRANT_THIRD = 10;
const int QUADRANT_FOURTH = 11;

const int VALUE_PAWN = 1000;
const int VALUE_KNIGHT = 3250;
const int VALUE_BISHOP = 3400;
const int VALUE_ROOK = 5000;
const int VALUE_QUEEN = 9000;

const int BONUS_STALEMATE = 500000;
const int BONUS_PAWN_FREE_COLUMN = 20;
const int BONUS_PAWN_FREE_FIELD = 10;
const int BONUS_PAWN_NEXT_TO_PAWN = 45;
const int BONUS_BISHOP_MORE = 100;
const int BONUS_BISHOP_MOBILITY = 15;   //per square
const int BONUS_ROOK_SEVENTH_RANK = 200;
const int BONUS_ROOK_DOUBLED_COLUMN = 150;
const int BONUS_ROOK_DOUBLED_ROW = 50;
const int BONUS_ROOK_COLUMN_OPEN = 100;
const int BONUS_ROOK_COLUMN_SEMIOPEN = 30;
const int BONUS_ROOK_MOBILITY = 5;  //per square
const int BONUS_QUEEN_MOBILITY = 10;    //per square
const int BONUS_QUEEN_NEAR_ENEMY_KING = 5; //per square
const int BONUS_KING_QUADRANT_DOMINANCE = 5;   //per chessman differrence
const int BONUS_CHECK = 600;

const int PENALTY_CHECKMATE = -500000;
const int PENALTY_PAWN_ISOLATED = -50;
const int PENALTY_PAWN_DOUBLED = -75;
const int PENALTY_QUEEN_EARLY_OUT = -400;
const int PENALTY_KING_NO_CASTLING = -140;
const int PENALTY_KING_NO_QUEENCASTLING = -90;
const int PENALTY_KING_NO_KINGCASTLING = -50;

const std::map<std::string,int> TABLE_PAWN_WHITE = {{std::string {COLUMN_A,ROW_1},0},{std::string {COLUMN_B,ROW_1},0},{std::string {COLUMN_C,ROW_1},0},{std::string {COLUMN_D,ROW_1},0},{std::string {COLUMN_E,ROW_1},0},{std::string {COLUMN_F,ROW_1},0},{std::string {COLUMN_G,ROW_1},0},{std::string {COLUMN_H,ROW_1},0},
                                                    {std::string {COLUMN_A,ROW_2},0},{std::string {COLUMN_B,ROW_2},0},{std::string {COLUMN_C,ROW_2},-20},{std::string {COLUMN_D,ROW_2},-160},{std::string {COLUMN_E,ROW_2},-160},{std::string {COLUMN_F,ROW_2},-20},{std::string {COLUMN_G,ROW_2},0},{std::string {COLUMN_H,ROW_2},0},
                                                    {std::string {COLUMN_A,ROW_3},20},{std::string {COLUMN_B,ROW_3},20},{std::string {COLUMN_C,ROW_3},20},{std::string {COLUMN_D,ROW_3},80},{std::string {COLUMN_E,ROW_3},80},{std::string {COLUMN_F,ROW_3},20},{std::string {COLUMN_G,ROW_3},20},{std::string {COLUMN_H,ROW_3},20},
                                                    {std::string {COLUMN_A,ROW_4},20},{std::string {COLUMN_B,ROW_4},20},{std::string {COLUMN_C,ROW_4},120},{std::string {COLUMN_D,ROW_4},160},{std::string {COLUMN_E,ROW_4},160},{std::string {COLUMN_F,ROW_4},120},{std::string {COLUMN_G,ROW_4},20},{std::string {COLUMN_H,ROW_4},20},
                                                    {std::string {COLUMN_A,ROW_5},80},{std::string {COLUMN_B,ROW_5},80},{std::string {COLUMN_C,ROW_5},120},{std::string {COLUMN_D,ROW_5},200},{std::string {COLUMN_E,ROW_5},200},{std::string {COLUMN_F,ROW_5},120},{std::string {COLUMN_G,ROW_5},80},{std::string {COLUMN_H,ROW_5},80},
                                                    {std::string {COLUMN_A,ROW_6},200},{std::string {COLUMN_B,ROW_6},200},{std::string {COLUMN_C,ROW_6},200},{std::string {COLUMN_D,ROW_6},300},{std::string {COLUMN_E,ROW_6},300},{std::string {COLUMN_F,ROW_6},200},{std::string {COLUMN_G,ROW_6},200},{std::string {COLUMN_H,ROW_6},200},
                                                    {std::string {COLUMN_A,ROW_7},400},{std::string {COLUMN_B,ROW_7},400},{std::string {COLUMN_C,ROW_7},400},{std::string {COLUMN_D,ROW_7},400},{std::string {COLUMN_E,ROW_7},400},{std::string {COLUMN_F,ROW_7},400},{std::string {COLUMN_G,ROW_7},400},{std::string {COLUMN_H,ROW_7},400},
                                                    {std::string {COLUMN_A,ROW_8},0},{std::string {COLUMN_B,ROW_8},0},{std::string {COLUMN_C,ROW_8},0},{std::string {COLUMN_D,ROW_8},0},{std::string {COLUMN_E,ROW_8},0},{std::string {COLUMN_F,ROW_8},0},{std::string {COLUMN_G,ROW_8},0},{std::string {COLUMN_H,ROW_8},0}};

const std::map<std::string,int> TABLE_PAWN_BLACK = {{std::string {COLUMN_A,ROW_8},0},{std::string {COLUMN_B,ROW_8},0},{std::string {COLUMN_C,ROW_8},0},{std::string {COLUMN_D,ROW_8},0},{std::string {COLUMN_E,ROW_8},0},{std::string {COLUMN_F,ROW_8},0},{std::string {COLUMN_G,ROW_8},0},{std::string {COLUMN_H,ROW_8},0},
                                                    {std::string {COLUMN_A,ROW_7},0},{std::string {COLUMN_B,ROW_7},0},{std::string {COLUMN_C,ROW_7},-20},{std::string {COLUMN_D,ROW_7},-160},{std::string {COLUMN_E,ROW_7},-160},{std::string {COLUMN_F,ROW_7},-20},{std::string {COLUMN_G,ROW_7},0},{std::string {COLUMN_H,ROW_7},0},
                                                    {std::string {COLUMN_A,ROW_6},20},{std::string {COLUMN_B,ROW_6},20},{std::string {COLUMN_C,ROW_6},20},{std::string {COLUMN_D,ROW_6},80},{std::string {COLUMN_E,ROW_6},80},{std::string {COLUMN_F,ROW_6},20},{std::string {COLUMN_G,ROW_6},20},{std::string {COLUMN_H,ROW_6},20},
                                                    {std::string {COLUMN_A,ROW_5},20},{std::string {COLUMN_B,ROW_5},20},{std::string {COLUMN_C,ROW_5},120},{std::string {COLUMN_D,ROW_5},160},{std::string {COLUMN_E,ROW_5},160},{std::string {COLUMN_F,ROW_5},120},{std::string {COLUMN_G,ROW_5},20},{std::string {COLUMN_H,ROW_5},20},
                                                    {std::string {COLUMN_A,ROW_4},80},{std::string {COLUMN_B,ROW_4},80},{std::string {COLUMN_C,ROW_4},120},{std::string {COLUMN_D,ROW_4},200},{std::string {COLUMN_E,ROW_4},200},{std::string {COLUMN_F,ROW_4},120},{std::string {COLUMN_G,ROW_4},80},{std::string {COLUMN_H,ROW_4},80},
                                                    {std::string {COLUMN_A,ROW_3},200},{std::string {COLUMN_B,ROW_3},200},{std::string {COLUMN_C,ROW_3},200},{std::string {COLUMN_D,ROW_3},300},{std::string {COLUMN_E,ROW_3},300},{std::string {COLUMN_F,ROW_3},200},{std::string {COLUMN_G,ROW_3},200},{std::string {COLUMN_H,ROW_3},200},
                                                    {std::string {COLUMN_A,ROW_2},400},{std::string {COLUMN_B,ROW_2},400},{std::string {COLUMN_C,ROW_2},400},{std::string {COLUMN_D,ROW_2},400},{std::string {COLUMN_E,ROW_2},400},{std::string {COLUMN_F,ROW_2},400},{std::string {COLUMN_G,ROW_2},400},{std::string {COLUMN_H,ROW_2},400},
                                                    {std::string {COLUMN_A,ROW_1},0},{std::string {COLUMN_B,ROW_1},0},{std::string {COLUMN_C,ROW_1},0},{std::string {COLUMN_D,ROW_1},0},{std::string {COLUMN_E,ROW_1},0},{std::string {COLUMN_F,ROW_1},0},{std::string {COLUMN_G,ROW_1},0},{std::string {COLUMN_H,ROW_1},0}};

const std::map<std::string,int> TABLE_KNIGHT_WHITE = {{std::string {COLUMN_A,ROW_1},-200},{std::string {COLUMN_B,ROW_1},-80},{std::string {COLUMN_C,ROW_1},-60},{std::string {COLUMN_D,ROW_1},-40},{std::string {COLUMN_E,ROW_1},-40},{std::string {COLUMN_F,ROW_1},-60},{std::string {COLUMN_G,ROW_1},-80},{std::string {COLUMN_H,ROW_1},-200},
                                                      {std::string {COLUMN_A,ROW_2},-100},{std::string {COLUMN_B,ROW_2},-100},{std::string {COLUMN_C,ROW_2},-60},{std::string {COLUMN_D,ROW_2},0},{std::string {COLUMN_E,ROW_2},0},{std::string {COLUMN_F,ROW_2},-60},{std::string {COLUMN_G,ROW_2},-100},{std::string {COLUMN_H,ROW_2},-100},
                                                      {std::string {COLUMN_A,ROW_3},-60},{std::string {COLUMN_B,ROW_3},0},{std::string {COLUMN_C,ROW_3},60},{std::string {COLUMN_D,ROW_3},100},{std::string {COLUMN_E,ROW_3},100},{std::string {COLUMN_F,ROW_3},60},{std::string {COLUMN_G,ROW_3},0},{std::string {COLUMN_H,ROW_3},-60},
                                                      {std::string {COLUMN_A,ROW_4},-40},{std::string {COLUMN_B,ROW_4},100},{std::string {COLUMN_C,ROW_4},100},{std::string {COLUMN_D,ROW_4},140},{std::string {COLUMN_E,ROW_4},140},{std::string {COLUMN_F,ROW_4},100},{std::string {COLUMN_G,ROW_4},100},{std::string {COLUMN_H,ROW_4},-40},
                                                      {std::string {COLUMN_A,ROW_5},0},{std::string {COLUMN_B,ROW_5},100},{std::string {COLUMN_C,ROW_5},120},{std::string {COLUMN_D,ROW_5},160},{std::string {COLUMN_E,ROW_5},160},{std::string {COLUMN_F,ROW_5},120},{std::string {COLUMN_G,ROW_5},100},{std::string {COLUMN_H,ROW_5},0},
                                                      {std::string {COLUMN_A,ROW_6},100},{std::string {COLUMN_B,ROW_6},120},{std::string {COLUMN_C,ROW_6},140},{std::string {COLUMN_D,ROW_6},160},{std::string {COLUMN_E,ROW_6},160},{std::string {COLUMN_F,ROW_6},140},{std::string {COLUMN_G,ROW_6},120},{std::string {COLUMN_H,ROW_6},100},
                                                      {std::string {COLUMN_A,ROW_7},0},{std::string {COLUMN_B,ROW_7},100},{std::string {COLUMN_C,ROW_7},120},{std::string {COLUMN_D,ROW_7},160},{std::string {COLUMN_E,ROW_7},160},{std::string {COLUMN_F,ROW_7},120},{std::string {COLUMN_G,ROW_7},100},{std::string {COLUMN_H,ROW_7},0},
                                                      {std::string {COLUMN_A,ROW_8},-200},{std::string {COLUMN_B,ROW_8},-100},{std::string {COLUMN_C,ROW_8},-40},{std::string {COLUMN_D,ROW_8},0},{std::string {COLUMN_E,ROW_8},0},{std::string {COLUMN_F,ROW_8},-40},{std::string {COLUMN_G,ROW_8},-100},{std::string {COLUMN_H,ROW_8},-200}};

const std::map<std::string,int> TABLE_KNIGHT_BLACK = {{std::string {COLUMN_A,ROW_8},-200},{std::string {COLUMN_B,ROW_8},-80},{std::string {COLUMN_C,ROW_8},-60},{std::string {COLUMN_D,ROW_8},-40},{std::string {COLUMN_E,ROW_8},-40},{std::string {COLUMN_F,ROW_8},-60},{std::string {COLUMN_G,ROW_8},-80},{std::string {COLUMN_H,ROW_8},-200},
                                                      {std::string {COLUMN_A,ROW_7},-100},{std::string {COLUMN_B,ROW_7},-100},{std::string {COLUMN_C,ROW_7},-60},{std::string {COLUMN_D,ROW_7},0},{std::string {COLUMN_E,ROW_7},0},{std::string {COLUMN_F,ROW_7},-60},{std::string {COLUMN_G,ROW_7},-100},{std::string {COLUMN_H,ROW_7},-100},
                                                      {std::string {COLUMN_A,ROW_6},-60},{std::string {COLUMN_B,ROW_6},0},{std::string {COLUMN_C,ROW_6},60},{std::string {COLUMN_D,ROW_6},100},{std::string {COLUMN_E,ROW_6},100},{std::string {COLUMN_F,ROW_6},60},{std::string {COLUMN_G,ROW_6},0},{std::string {COLUMN_H,ROW_6},-60},
                                                      {std::string {COLUMN_A,ROW_5},-40},{std::string {COLUMN_B,ROW_5},100},{std::string {COLUMN_C,ROW_5},100},{std::string {COLUMN_D,ROW_5},140},{std::string {COLUMN_E,ROW_5},140},{std::string {COLUMN_F,ROW_5},100},{std::string {COLUMN_G,ROW_5},100},{std::string {COLUMN_H,ROW_5},-40},
                                                      {std::string {COLUMN_A,ROW_4},0},{std::string {COLUMN_B,ROW_4},100},{std::string {COLUMN_C,ROW_4},120},{std::string {COLUMN_D,ROW_4},160},{std::string {COLUMN_E,ROW_4},160},{std::string {COLUMN_F,ROW_4},120},{std::string {COLUMN_G,ROW_4},100},{std::string {COLUMN_H,ROW_4},0},
                                                      {std::string {COLUMN_A,ROW_3},100},{std::string {COLUMN_B,ROW_3},120},{std::string {COLUMN_C,ROW_3},140},{std::string {COLUMN_D,ROW_3},160},{std::string {COLUMN_E,ROW_3},160},{std::string {COLUMN_F,ROW_3},140},{std::string {COLUMN_G,ROW_3},120},{std::string {COLUMN_H,ROW_3},100},
                                                      {std::string {COLUMN_A,ROW_2},0},{std::string {COLUMN_B,ROW_2},100},{std::string {COLUMN_C,ROW_2},120},{std::string {COLUMN_D,ROW_2},160},{std::string {COLUMN_E,ROW_2},160},{std::string {COLUMN_F,ROW_2},120},{std::string {COLUMN_G,ROW_2},100},{std::string {COLUMN_H,ROW_2},0},
                                                      {std::string {COLUMN_A,ROW_1},-200},{std::string {COLUMN_B,ROW_1},-100},{std::string {COLUMN_C,ROW_1},-40},{std::string {COLUMN_D,ROW_1},0},{std::string {COLUMN_E,ROW_1},0},{std::string {COLUMN_F,ROW_1},-40},{std::string {COLUMN_G,ROW_1},-100},{std::string {COLUMN_H,ROW_1},-200}};

const std::map<std::string,int> TABLE_BISHOP_WHITE = {{std::string {COLUMN_A,ROW_1},-100},{std::string {COLUMN_B,ROW_1},-80},{std::string {COLUMN_C,ROW_1},-60},{std::string {COLUMN_D,ROW_1},-40},{std::string {COLUMN_E,ROW_1},-40},{std::string {COLUMN_F,ROW_1},-60},{std::string {COLUMN_G,ROW_1},-80},{std::string {COLUMN_H,ROW_1},-100},
                                                      {std::string {COLUMN_A,ROW_2},-100},{std::string {COLUMN_B,ROW_2},120},{std::string {COLUMN_C,ROW_2},-40},{std::string {COLUMN_D,ROW_2},100},{std::string {COLUMN_E,ROW_2},100},{std::string {COLUMN_F,ROW_2},-40},{std::string {COLUMN_G,ROW_2},120},{std::string {COLUMN_H,ROW_2},-100},
                                                      {std::string {COLUMN_A,ROW_3},0},{std::string {COLUMN_B,ROW_3},0},{std::string {COLUMN_C,ROW_3},20},{std::string {COLUMN_D,ROW_3},100},{std::string {COLUMN_E,ROW_3},100},{std::string {COLUMN_F,ROW_3},20},{std::string {COLUMN_G,ROW_3},0},{std::string {COLUMN_H,ROW_3},0},
                                                      {std::string {COLUMN_A,ROW_4},0},{std::string {COLUMN_B,ROW_4},40},{std::string {COLUMN_C,ROW_4},100},{std::string {COLUMN_D,ROW_4},100},{std::string {COLUMN_E,ROW_4},100},{std::string {COLUMN_F,ROW_4},100},{std::string {COLUMN_G,ROW_4},40},{std::string {COLUMN_H,ROW_4},0},
                                                      {std::string {COLUMN_A,ROW_5},0},{std::string {COLUMN_B,ROW_5},100},{std::string {COLUMN_C,ROW_5},120},{std::string {COLUMN_D,ROW_5},160},{std::string {COLUMN_E,ROW_5},160},{std::string {COLUMN_F,ROW_5},120},{std::string {COLUMN_G,ROW_5},100},{std::string {COLUMN_H,ROW_5},0},
                                                      {std::string {COLUMN_A,ROW_6},0},{std::string {COLUMN_B,ROW_6},0},{std::string {COLUMN_C,ROW_6},0},{std::string {COLUMN_D,ROW_6},0},{std::string {COLUMN_E,ROW_6},0},{std::string {COLUMN_F,ROW_6},0},{std::string {COLUMN_G,ROW_6},0},{std::string {COLUMN_H,ROW_6},0},
                                                      {std::string {COLUMN_A,ROW_7},0},{std::string {COLUMN_B,ROW_7},0},{std::string {COLUMN_C,ROW_7},0},{std::string {COLUMN_D,ROW_7},0},{std::string {COLUMN_E,ROW_7},0},{std::string {COLUMN_F,ROW_7},0},{std::string {COLUMN_G,ROW_7},0},{std::string {COLUMN_H,ROW_7},0},
                                                      {std::string {COLUMN_A,ROW_8},-100},{std::string {COLUMN_B,ROW_8},0},{std::string {COLUMN_C,ROW_8},0},{std::string {COLUMN_D,ROW_8},0},{std::string {COLUMN_E,ROW_8},0},{std::string {COLUMN_F,ROW_8},0},{std::string {COLUMN_G,ROW_8},0},{std::string {COLUMN_H,ROW_8},-100}};

const std::map<std::string,int> TABLE_BISHOP_BLACK = {{std::string {COLUMN_A,ROW_8},-100},{std::string {COLUMN_B,ROW_8},-80},{std::string {COLUMN_C,ROW_8},-60},{std::string {COLUMN_D,ROW_8},-40},{std::string {COLUMN_E,ROW_8},-40},{std::string {COLUMN_F,ROW_8},-60},{std::string {COLUMN_G,ROW_8},-80},{std::string {COLUMN_H,ROW_8},-100},
                                                      {std::string {COLUMN_A,ROW_7},-100},{std::string {COLUMN_B,ROW_7},120},{std::string {COLUMN_C,ROW_7},-40},{std::string {COLUMN_D,ROW_7},100},{std::string {COLUMN_E,ROW_7},100},{std::string {COLUMN_F,ROW_7},-40},{std::string {COLUMN_G,ROW_7},120},{std::string {COLUMN_H,ROW_7},-100},
                                                      {std::string {COLUMN_A,ROW_6},0},{std::string {COLUMN_B,ROW_6},0},{std::string {COLUMN_C,ROW_6},20},{std::string {COLUMN_D,ROW_6},100},{std::string {COLUMN_E,ROW_6},100},{std::string {COLUMN_F,ROW_6},20},{std::string {COLUMN_G,ROW_6},0},{std::string {COLUMN_H,ROW_6},0},
                                                      {std::string {COLUMN_A,ROW_5},0},{std::string {COLUMN_B,ROW_5},40},{std::string {COLUMN_C,ROW_5},100},{std::string {COLUMN_D,ROW_5},100},{std::string {COLUMN_E,ROW_5},100},{std::string {COLUMN_F,ROW_5},100},{std::string {COLUMN_G,ROW_5},40},{std::string {COLUMN_H,ROW_5},0},
                                                      {std::string {COLUMN_A,ROW_4},0},{std::string {COLUMN_B,ROW_4},100},{std::string {COLUMN_C,ROW_4},120},{std::string {COLUMN_D,ROW_4},160},{std::string {COLUMN_E,ROW_4},160},{std::string {COLUMN_F,ROW_4},120},{std::string {COLUMN_G,ROW_4},100},{std::string {COLUMN_H,ROW_4},0},
                                                      {std::string {COLUMN_A,ROW_3},0},{std::string {COLUMN_B,ROW_3},0},{std::string {COLUMN_C,ROW_3},0},{std::string {COLUMN_D,ROW_3},0},{std::string {COLUMN_E,ROW_3},0},{std::string {COLUMN_F,ROW_3},0},{std::string {COLUMN_G,ROW_3},0},{std::string {COLUMN_H,ROW_3},0},
                                                      {std::string {COLUMN_A,ROW_2},0},{std::string {COLUMN_B,ROW_2},0},{std::string {COLUMN_C,ROW_2},0},{std::string {COLUMN_D,ROW_2},0},{std::string {COLUMN_E,ROW_2},0},{std::string {COLUMN_F,ROW_2},0},{std::string {COLUMN_G,ROW_2},0},{std::string {COLUMN_H,ROW_2},0},
                                                      {std::string {COLUMN_A,ROW_1},-100},{std::string {COLUMN_B,ROW_1},0},{std::string {COLUMN_C,ROW_1},0},{std::string {COLUMN_D,ROW_1},0},{std::string {COLUMN_E,ROW_1},0},{std::string {COLUMN_F,ROW_1},0},{std::string {COLUMN_G,ROW_1},0},{std::string {COLUMN_H,ROW_1},-100}};

const std::map<std::string,int> TABLE_KING_WHITE = {{std::string {COLUMN_A,ROW_1},200},{std::string {COLUMN_B,ROW_1},300},{std::string {COLUMN_C,ROW_1},260},{std::string {COLUMN_D,ROW_1},-100},{std::string {COLUMN_E,ROW_1},0},{std::string {COLUMN_F,ROW_1},-100},{std::string {COLUMN_G,ROW_1},340},{std::string {COLUMN_H,ROW_8},240},
                                                    {std::string {COLUMN_A,ROW_2},-100},{std::string {COLUMN_B,ROW_2},-100},{std::string {COLUMN_C,ROW_2},-200},{std::string {COLUMN_D,ROW_2},-300},{std::string {COLUMN_E,ROW_2},-300},{std::string {COLUMN_F,ROW_2},-200},{std::string {COLUMN_G,ROW_2},-100},{std::string {COLUMN_H,ROW_7},-100},
                                                    {std::string {COLUMN_A,ROW_3},-300},{std::string {COLUMN_B,ROW_3},-300},{std::string {COLUMN_C,ROW_3},-360},{std::string {COLUMN_D,ROW_3},-400},{std::string {COLUMN_E,ROW_3},-400},{std::string {COLUMN_F,ROW_3},-360},{std::string {COLUMN_G,ROW_3},-300},{std::string {COLUMN_H,ROW_6},-300},
                                                    {std::string {COLUMN_A,ROW_4},-600},{std::string {COLUMN_B,ROW_4},-600},{std::string {COLUMN_C,ROW_4},-600},{std::string {COLUMN_D,ROW_4},-600},{std::string {COLUMN_E,ROW_4},-600},{std::string {COLUMN_F,ROW_4},-600},{std::string {COLUMN_G,ROW_4},-600},{std::string {COLUMN_H,ROW_5},-600},
                                                    {std::string {COLUMN_A,ROW_5},-600},{std::string {COLUMN_B,ROW_5},-600},{std::string {COLUMN_C,ROW_5},-600},{std::string {COLUMN_D,ROW_5},-600},{std::string {COLUMN_E,ROW_5},-600},{std::string {COLUMN_F,ROW_5},-600},{std::string {COLUMN_G,ROW_5},-600},{std::string {COLUMN_H,ROW_4},-600},
                                                    {std::string {COLUMN_A,ROW_6},-600},{std::string {COLUMN_B,ROW_6},-600},{std::string {COLUMN_C,ROW_6},-600},{std::string {COLUMN_D,ROW_6},-600},{std::string {COLUMN_E,ROW_6},-600},{std::string {COLUMN_F,ROW_6},-600},{std::string {COLUMN_G,ROW_6},-600},{std::string {COLUMN_H,ROW_3},-600},
                                                    {std::string {COLUMN_A,ROW_7},-600},{std::string {COLUMN_B,ROW_7},-600},{std::string {COLUMN_C,ROW_7},-600},{std::string {COLUMN_D,ROW_7},-600},{std::string {COLUMN_E,ROW_7},-600},{std::string {COLUMN_F,ROW_7},-600},{std::string {COLUMN_G,ROW_7},-600},{std::string {COLUMN_H,ROW_2},-600},
                                                    {std::string {COLUMN_A,ROW_8},-600},{std::string {COLUMN_B,ROW_8},-600},{std::string {COLUMN_C,ROW_8},-600},{std::string {COLUMN_D,ROW_8},-600},{std::string {COLUMN_E,ROW_8},-600},{std::string {COLUMN_F,ROW_8},-600},{std::string {COLUMN_G,ROW_8},-600},{std::string {COLUMN_H,ROW_1},-600}};


const std::map<std::string,int> TABLE_KING_BLACK = {{std::string {COLUMN_A,ROW_8},200},{std::string {COLUMN_B,ROW_8},300},{std::string {COLUMN_C,ROW_8},260},{std::string {COLUMN_D,ROW_8},-100},{std::string {COLUMN_E,ROW_8},0},{std::string {COLUMN_F,ROW_8},-100},{std::string {COLUMN_G,ROW_8},340},{std::string {COLUMN_H,ROW_8},240},
                                                    {std::string {COLUMN_A,ROW_7},-100},{std::string {COLUMN_B,ROW_7},-100},{std::string {COLUMN_C,ROW_7},-200},{std::string {COLUMN_D,ROW_7},-300},{std::string {COLUMN_E,ROW_7},-300},{std::string {COLUMN_F,ROW_7},-200},{std::string {COLUMN_G,ROW_7},-100},{std::string {COLUMN_H,ROW_7},-100},
                                                    {std::string {COLUMN_A,ROW_6},-300},{std::string {COLUMN_B,ROW_6},-300},{std::string {COLUMN_C,ROW_6},-360},{std::string {COLUMN_D,ROW_6},-400},{std::string {COLUMN_E,ROW_6},-400},{std::string {COLUMN_F,ROW_6},-360},{std::string {COLUMN_G,ROW_6},-300},{std::string {COLUMN_H,ROW_6},-300},
                                                    {std::string {COLUMN_A,ROW_5},-600},{std::string {COLUMN_B,ROW_5},-600},{std::string {COLUMN_C,ROW_5},-600},{std::string {COLUMN_D,ROW_5},-600},{std::string {COLUMN_E,ROW_5},-600},{std::string {COLUMN_F,ROW_5},-600},{std::string {COLUMN_G,ROW_5},-600},{std::string {COLUMN_H,ROW_5},-600},
                                                    {std::string {COLUMN_A,ROW_4},-600},{std::string {COLUMN_B,ROW_4},-600},{std::string {COLUMN_C,ROW_4},-600},{std::string {COLUMN_D,ROW_4},-600},{std::string {COLUMN_E,ROW_4},-600},{std::string {COLUMN_F,ROW_4},-600},{std::string {COLUMN_G,ROW_4},-600},{std::string {COLUMN_H,ROW_4},-600},
                                                    {std::string {COLUMN_A,ROW_3},-600},{std::string {COLUMN_B,ROW_3},-600},{std::string {COLUMN_C,ROW_3},-600},{std::string {COLUMN_D,ROW_3},-600},{std::string {COLUMN_E,ROW_3},-600},{std::string {COLUMN_F,ROW_3},-600},{std::string {COLUMN_G,ROW_3},-600},{std::string {COLUMN_H,ROW_3},-600},
                                                    {std::string {COLUMN_A,ROW_2},-600},{std::string {COLUMN_B,ROW_2},-600},{std::string {COLUMN_C,ROW_2},-600},{std::string {COLUMN_D,ROW_2},-600},{std::string {COLUMN_E,ROW_2},-600},{std::string {COLUMN_F,ROW_2},-600},{std::string {COLUMN_G,ROW_2},-600},{std::string {COLUMN_H,ROW_2},-600},
                                                    {std::string {COLUMN_A,ROW_1},-600},{std::string {COLUMN_B,ROW_1},-600},{std::string {COLUMN_C,ROW_1},-600},{std::string {COLUMN_D,ROW_1},-600},{std::string {COLUMN_E,ROW_1},-600},{std::string {COLUMN_F,ROW_1},-600},{std::string {COLUMN_G,ROW_1},-600},{std::string {COLUMN_H,ROW_1},-600}};

const std::map<std::string,int> TABLE_KING_ENDGAME = {{std::string {COLUMN_A,ROW_8},-400},{std::string {COLUMN_B,ROW_8},-300},{std::string {COLUMN_C,ROW_8},-240},{std::string {COLUMN_D,ROW_8},-200},{std::string {COLUMN_E,ROW_8},-200},{std::string {COLUMN_F,ROW_8},-240},{std::string {COLUMN_G,ROW_8},-300},{std::string {COLUMN_H,ROW_8},-400},
                                                      {std::string {COLUMN_A,ROW_7},-300},{std::string {COLUMN_B,ROW_7},-240},{std::string {COLUMN_C,ROW_7},-140},{std::string {COLUMN_D,ROW_7},0},{std::string {COLUMN_E,ROW_7},0},{std::string {COLUMN_F,ROW_7},-140},{std::string {COLUMN_G,ROW_7},-240},{std::string {COLUMN_H,ROW_7},-300},
                                                      {std::string {COLUMN_A,ROW_6},-240},{std::string {COLUMN_B,ROW_6},-140},{std::string {COLUMN_C,ROW_6},20},{std::string {COLUMN_D,ROW_6},100},{std::string {COLUMN_E,ROW_6},100},{std::string {COLUMN_F,ROW_6},20},{std::string {COLUMN_G,ROW_6},-140},{std::string {COLUMN_H,ROW_6},-240},
                                                      {std::string {COLUMN_A,ROW_5},-200},{std::string {COLUMN_B,ROW_5},0},{std::string {COLUMN_C,ROW_5},100},{std::string {COLUMN_D,ROW_5},200},{std::string {COLUMN_E,ROW_5},200},{std::string {COLUMN_F,ROW_5},100},{std::string {COLUMN_G,ROW_5},0},{std::string {COLUMN_H,ROW_5},-200},
                                                      {std::string {COLUMN_A,ROW_4},-200},{std::string {COLUMN_B,ROW_4},0},{std::string {COLUMN_C,ROW_4},100},{std::string {COLUMN_D,ROW_4},200},{std::string {COLUMN_E,ROW_4},200},{std::string {COLUMN_F,ROW_4},100},{std::string {COLUMN_G,ROW_4},0},{std::string {COLUMN_H,ROW_4},-200},
                                                      {std::string {COLUMN_A,ROW_3},-240},{std::string {COLUMN_B,ROW_3},-140},{std::string {COLUMN_C,ROW_3},20},{std::string {COLUMN_D,ROW_3},100},{std::string {COLUMN_E,ROW_3},100},{std::string {COLUMN_F,ROW_3},20},{std::string {COLUMN_G,ROW_3},-140},{std::string {COLUMN_H,ROW_3},-240},
                                                      {std::string {COLUMN_A,ROW_2},-300},{std::string {COLUMN_B,ROW_2},-240},{std::string {COLUMN_C,ROW_2},-140},{std::string {COLUMN_D,ROW_2},0},{std::string {COLUMN_E,ROW_2},0},{std::string {COLUMN_F,ROW_2},-140},{std::string {COLUMN_G,ROW_2},-240},{std::string {COLUMN_H,ROW_2},-300},
                                                      {std::string {COLUMN_A,ROW_1},-400},{std::string {COLUMN_B,ROW_1},-300},{std::string {COLUMN_C,ROW_1},-240},{std::string {COLUMN_D,ROW_1},-200},{std::string {COLUMN_E,ROW_1},-200},{std::string {COLUMN_F,ROW_1},-240},{std::string {COLUMN_G,ROW_1},-300},{std::string {COLUMN_H,ROW_1},-400}};