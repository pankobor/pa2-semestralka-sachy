/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 3. 5. 2020
 * inspired by: https://gitlab.fit.cvut.cz/bernhdav/pa2-minesweeper/blob/master/src/CInterface.cpp
 */

#include "CInterface.h"

using namespace std;

//public----------------------------------------------------------------------------------------------------------------
CInterface::CInterface(std::istream &in, std::ostream &out) : m_In(in), m_Out(out) {
    m_In.exceptions(std::ios::badbit | std::ios::eofbit);
    m_Out.exceptions(std::ios::failbit | std::ios::badbit | std::ios::eofbit);
}


CInterface::CInterface(const CInterface &src) : m_In(src.m_In), m_Out(src.m_Out) {
    m_In.exceptions(std::ios::badbit | std::ios::eofbit);
    m_Out.exceptions(std::ios::failbit | std::ios::badbit | std::ios::eofbit);
}

std::string
CInterface::promptCommand(const function<bool(const string &)> &valid, const CGame &game, string &params) const {
    string command;
    while (true) {
        if (game.isRunning()) {
            //when game is running the interface is different and command is prompted by dynamic binding
            //because one of the players may be AI
            m_Out << (game.getMoveCount() % 2 == 0 ? "White (" : "Black (") << game.getMoveCount() / 2 + 1 << ") : ";
            command = game.getPlayer(game.getMoveCount() % 2 == 0)->requestMove(m_In, m_Out, game.getBoard(), params);
        } else {
            m_Out << ">";
            if (!(m_In >> command)) {
                m_Out << ERROR_INVALID_INPUT << endl;
            }
            //skip the blank characters before command parameters
            while (m_In.peek() == ' ') {
                m_In.get();
            }
            if (m_In.peek() != '\n') {
                m_In >> params;
            }
        }
        if (!valid(command)) {
            m_Out << ERROR_INVALID_COMMAND << endl;
            this->clearLine();
        } else {
            this->clearLine();
            return command;
        }
    }
}

char CInterface::promptWithOptions(const string &text, const string &choices) const {
    char command;
    while (true) {
        m_Out << text;
        if (!(m_In >> command)) {
            m_Out << ERROR_INVALID_INPUT << endl;
        }
        //testing that user input is one of possible choices
        for (char c : choices) {
            if (c == command) {
                this->clearLine();
                return command;
            }
        }
        m_Out << ERROR_INVALID_CHOICE << endl;
        this->clearLine();
    }
}

void CInterface::printHelp(const string &command, const string &help) const {
    m_Out << command << ":  " << help << endl;
}

void CInterface::print(const string &message) const {
    m_Out << message << endl;
}

void CInterface::printBoard(const CGame &game) const {
    game.getBoard().print(m_Out);
}

void CInterface::printEvent(int ending, const CGame &game) const {
    if (ending == FLAG_CHECK) {
        m_Out << EVENT_CHECK;
    } else if (ending == FLAG_CHECKMATE) {
        m_Out << EVENT_CHECKMATE << endl;
        if (game.getMoveCount() % 2 == 0) {
            m_Out << EVENT_WHITE_WIN << endl;
        } else {
            m_Out << EVENT_BLACK_WIN << endl;
        }
        //if checkmate occurred the message "You win"/"Youlost" is written by AI depending on if the AI won
        //as we don't know which player is AI, we will call endMessage for both using dynamic binding , the CPerson will do nothing
        game.getPlayer(true)->endMessage(m_Out, game.getMoveCount() % 2 == 0);    //white wins
        game.getPlayer(false)->endMessage(m_Out, game.getMoveCount() % 2 == 0);
    } else if (ending == FLAG_STALEMATE) {
        m_Out << EVENT_STALEMATE << endl;
    } else {    //one of the players surrendered
        if (game.getMoveCount() % 2 == 0) {
            m_Out << EVENT_BLACK_WIN << endl;
        } else {
            m_Out << EVENT_WHITE_WIN << endl;
        }
        game.getPlayer(true)->endMessage(m_Out, game.getMoveCount() % 2 != 0);
        game.getPlayer(false)->endMessage(m_Out, game.getMoveCount() % 2 != 0);
    }
}

//private---------------------------------------------------------------------------------------------------------------
void CInterface::clearLine() const {
    m_In.clear();
    m_In.ignore(numeric_limits<streamsize>::max(), '\n');
}