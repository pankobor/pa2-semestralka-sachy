/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 9. 5. 2020
 */

#pragma once

#include "CPlayer.h"

class CPerson : public CPlayer {
public:
    /**
     * Writes CPerson mark to a savefile, so it can be then distinguished from AI
     * @param ofs representing the output filestream to savefile
     * @return true if saving was succesful and the output file stream is OK
     */
    bool save(std::ofstream &ofs) const override;

    /** endMessage has no function for real player */
    void endMessage(std::ostream &, bool) const override {}

    /** @return new unique_ptr to CPlayer*/
    std::unique_ptr<CPlayer> clone() const override;

    /**
     * Gets a command and its parameters from istream that the real person is writing to
     * @param in used to get an input from person
     * @param out to warn user if he wrote some invalid input to in
     * @param board is not used for CPerson, because real player see the board in console
     * @param params output parameter to store a parameters for command
     * @return command from user
     */
    std::string requestMove(std::istream &in, std::ostream &out, const CBoard &board, std::string &params) override;
};
