/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 28. 5. 2020
 * values and rules of evaluation inspired by:
 *      https://www.dailychess.com/rival/programming/evaluation.php
 *      https://web.archive.org/web/20111025132443/http://www.linuxsoft.cz/article.php?id_article=1659
 */

#pragma once

#include <string>
#include <map>
#include "CChessman.h"
#include "TCoord.h"
#include "constants.h"

class CStaticBoardEvaluator {
    /**
     * Calculate static position value depending on given rules, bonuses and penalties. Does NOT consider future or former moves.
     * Calculates a value of players material (that means, each chessman has its value independently of position) plus
     * positional value of chessmen. The positional part should be ideally less then value of pawn, to encourage the AI
     * to try to eliminate enemy resources while still trying to move it's own material to good positions.
     * Besides running method for each chessman, calculates also some bonuses and penalties that depends on whole situation:
     *      1. Small bonus if player has more than 1 bishops. That means, 2 bishops are worth more than 2*(value of bishop)
     *      2. Penalty if the queen is out too early to prevent AI from gambling with it
     *      3. Bonus if the player has dominance in the sector where is his king
     *      4. Bonus for having 2 rooks on same column or row. Also bonus if at least one rook is at 7th (for white) row
     *      5. Penalty for having more than one pawn in a column.
     *      6. For king, bishop, knight and pawn add a bonus or penalty depending on his coordinates.
     * @param fields representing the board
     * @param white indicates for which player is counted the value
     * @param game_phase
     * @return position value of one player
     */
    static int oneSideValue(const std::map<TCoord, CChessman> &fields, bool white, int game_phase);

    /**
     * Decides the game phase from material and positional rules. The middle-game starts if at least 2 minor or major
     * chessmen were taken, or if number of occupied home fields are lower than 20. The end-game starts if there are
     * less than minor and major chessmen left.
     * @param fields all fields at the chess board with it's chessman
     * @return phase of the game depending on overall situation
     */
    static int gamePhase(const std::map<TCoord, CChessman> &fields);

    /**
     * Updates the quadrants value depending on where the field is, the color and type of chessman at the given field
     * @param field indicates position and chessman. Queen is for 3 points, others are for 1 point. King isn't counted.
     * @param white indicates player. For chessmen that belongs to player the points are added, otherwise substracted.
     * @param q1 output parameter with value of quadrant 1
     * @param q2 output parameter with value of quadrant 2
     * @param q3 output parameter with value of quadrant 3
     * @param q4 output parameter with value of quadrant 4
     */
    static void
    updateQuadrantValue(const std::pair<TCoord, CChessman> &field, bool white, int &q1, int &q2, int &q3, int &q4);

    /**
     * Calculates value of pawn depending on his position with these rules:
     *      1.  Bonus if the pawn stands next to friendly pawn. Prevents AI from going massively diagonaly with pawns.
     *      2.  Bonus if the pawn has free way to the column end and small bonus if the fiel right in front of pawn is
     *          free. Encourages AI to move with pawns that have best chances for promotion.
     *      3.  Penalty if the pawn is isolated.
     * @param white indicates the player
     * @param fields all fields at board
     * @param field position of pawn
     * @return value of pawn depending on its position and situation around him
     */
    static int
    evaluatePawn(const std::map<TCoord, CChessman> &fields, const std::pair<TCoord, CChessman> &field, bool white);

    /**
     * Calculates a bonus for each file that the bishop is able to go to.
     * @param white indicates the player
     * @param fields all fields at board
     * @param field position of bishop
     * @return value of bishop depending on its position and situation around him
     */
    static int
    evaluateBishop(const std::map<TCoord, CChessman> &fields, const std::pair<TCoord, CChessman> &field, bool white);

    /**
     * Calculates a bonus for each file that the rook is able to go to. Also a bonus if the column or row is free.
     * @param white indicates the player
     * @param fields all fields at board
     * @param field position of rook
     * @return value of rook depending on its position and situation around him
     */
    static int
    evaluateRook(const std::map<TCoord, CChessman> &fields, const std::pair<TCoord, CChessman> &field, bool white);

    /**
     * Calculates a bonus for each file that the queen is able to go to. Also a bonus for queens closeness to enemy king
     * @param white indicates the player
     * @param fields all fields at board
     * @param field position of queen
     * @return value of queen depending on its position and situation around him
     */
    static int
    evaluateQueen(const std::map<TCoord, CChessman> &fields, const std::pair<TCoord, CChessman> &field, bool white);

    /**
     * Calculates a penalty if one or both types of castling aren't possible.
     * @param white indicates the player
     * @param fields all fields at board
     * @param field position of king
     * @return value of king depending on its position and situation around him
     */
    static int
    evaluateKing(const std::map<TCoord, CChessman> &fields, const std::pair<TCoord, CChessman> &field, bool white);

public:
    CStaticBoardEvaluator() = default;

    ~CStaticBoardEvaluator() = default;

    /**
     * @param fields representing all fields in chess board
     * @param white representing which player is on the move
     * @return value of position of player who is on the move minus value of position of the enemy player
     */
    static int calculatePositionValue(const std::map<TCoord, CChessman> &fields, bool white);
};