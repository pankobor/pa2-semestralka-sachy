/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 1. 5. 2020
 */

#include <iostream>
#include "CApplication.h"

using namespace std;

int main() {
    try {
        return CApplication{CInterface{cin, cout}}.Run();
    } catch (ios::failure &fail) {
        if (cin.eof()) {
            return 0;
        }
        cerr << fail.what() << endl;
        return 64;
    }
}
