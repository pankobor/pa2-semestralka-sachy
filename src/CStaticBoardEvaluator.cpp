/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 28. 5. 2020
 */

#include "CStaticBoardEvaluator.h"

using namespace std;

//public----------------------------------------------------------------------------------------------------------------
int CStaticBoardEvaluator::calculatePositionValue(const map<TCoord, CChessman> &fields, bool white) {
    const int game_phase = gamePhase(fields);
    return oneSideValue(fields, white, game_phase) - oneSideValue(fields, !white, game_phase);
}

//private---------------------------------------------------------------------------------------------------------------
int CStaticBoardEvaluator::oneSideValue(const map<TCoord, CChessman> &fields, bool white, const int game_phase) {
    int final_value = 0;
    int bishops_count = 0;
    int q1 = 0, q2 = 0, q3 = 0, q4 = 0;
    int *q;
    for (const auto &field : fields) {
        if (field.second.isWhite() == white && field.second.getMark() != MARK_EMPTY) {
            if (field.second.getMark() == MARK_PAWN) {
                final_value += VALUE_PAWN;
                final_value += evaluatePawn(fields, field, white);
            } else if (field.second.getMark() == MARK_KNIGHT) {
                final_value += VALUE_KNIGHT;
                final_value += white ? TABLE_KNIGHT_WHITE.at(field.first.toString()) : TABLE_KNIGHT_BLACK.at(
                        field.first.toString());
            } else if (field.second.getMark() == MARK_BISHOP) {
                final_value += VALUE_BISHOP;
                bishops_count++;
                final_value += evaluateBishop(fields, field, white);
            } else if (field.second.getMark() == MARK_ROOK) {
                final_value += VALUE_ROOK;
                final_value += evaluateRook(fields, field, white);
            } else if (field.second.getMark() == MARK_QUEEN) {
                final_value += VALUE_QUEEN;
                final_value += evaluateQueen(fields, field, white);
                if (game_phase == PHASE_EARLY &&
                    ((field.first.row > ROW_1 && white) || (field.first.row < ROW_8 && !white))) {
                    final_value += PENALTY_QUEEN_EARLY_OUT;
                }
            } else {
                if (game_phase != PHASE_END) {
                    final_value += white ? TABLE_KING_WHITE.at(field.first.toString()) : TABLE_KING_BLACK.at(
                            field.first.toString());
                } else {
                    final_value += TABLE_KING_ENDGAME.at(field.first.toString());
                }
                final_value += evaluateKing(fields, field, white);
                //when it finds the king set the pointer on the quadrant with king
                if (field.first.getQuadrant() == QUADRANT_FIRST) {
                    q = &q1;
                } else if (field.first.getQuadrant() == QUADRANT_SECOND) {
                    q = &q2;
                } else if (field.first.getQuadrant() == QUADRANT_THIRD) {
                    q = &q3;
                } else {
                    q = &q4;
                }
            }
        }
        if (field.second.getMark() != MARK_EMPTY && field.second.getMark() != MARK_KING) {
            updateQuadrantValue(field, white, q1, q2, q3, q4);
        }
    }
    bool doubled_pawns = false;
    bool rooks_col = false;
    bool rook7th = false;
    for (char c : string{COLUMNS}) {
        int column_pawns = 0;
        int column_rooks = 0;
        for (char r : string{ROWS}) {
            if (fields.at(TCoord{c, r}).getMark() == MARK_PAWN && fields.at(TCoord{c, r}).isWhite() == white) {
                column_pawns++;
            }
            if (fields.at(TCoord{c, r}).getMark() == MARK_ROOK && fields.at(TCoord{c, r}).isWhite() == white) {
                column_rooks++;
                if ((r == ROW_7 && white) || (r == ROW_2 && !white)) {
                    rook7th = true;
                }
            }
        }
        if (column_rooks > 1) {
            rooks_col = true;
        }
        if (column_pawns > 1) {
            doubled_pawns = true;
            break;
        }
    }
    if (rook7th) {
        final_value += BONUS_ROOK_SEVENTH_RANK;
    }
    if (rooks_col) {
        final_value += BONUS_ROOK_DOUBLED_COLUMN;
    } else {
        for (char r : string{ROWS}) {
            int row_rooks = 0;
            for (char c : string{COLUMNS}) {
                if (fields.at(TCoord{c, r}).getMark() == MARK_ROOK && fields.at(TCoord{c, r}).isWhite() == white) {
                    row_rooks++;
                }
                if (row_rooks > 1) {
                    final_value += BONUS_ROOK_DOUBLED_ROW;
                    break;
                }
            }
        }
    }
    if (doubled_pawns) {
        final_value += PENALTY_PAWN_DOUBLED;
    }
    if (bishops_count > 1) {
        final_value += BONUS_BISHOP_MORE;
    }
    final_value += (*q) * BONUS_KING_QUADRANT_DOMINANCE;
    return final_value;
}


int CStaticBoardEvaluator::gamePhase(const map<TCoord, CChessman> &fields) {
    int minor_count = 0;
    int major_count = 0;
    int homefields_occupied = 0;
    for (const auto &field : fields) {
        if (field.second.getMark() == MARK_KNIGHT || field.second.getMark() == MARK_BISHOP) {
            minor_count++;
        } else if (field.second.getMark() == MARK_ROOK || field.second.getMark() == MARK_QUEEN) {
            major_count++;
        }
        if ((field.first.row > ROW_6 || field.first.row < ROW_3) && field.second.getMark() != MARK_EMPTY) {
            homefields_occupied++;
        }
    }
    if (minor_count + major_count < 7) {
        return PHASE_END;
    } else if ((homefields_occupied < 20) || (minor_count + major_count < 13)) {
        return PHASE_MIDDLE;
    }
    return PHASE_EARLY;
}

void
CStaticBoardEvaluator::updateQuadrantValue(const pair<TCoord, CChessman> &field, bool white, int &q1, int &q2, int &q3,
                                           int &q4) {
    int add = 1;
    if (field.second.isWhite() != white) {
        add = -1;
    }
    if (field.first.getQuadrant() == QUADRANT_FIRST) {
        field.second.getMark() == MARK_QUEEN ? q1 += 3 * add : q1 += add;
    } else if (field.first.getQuadrant() == QUADRANT_FIRST) {
        field.second.getMark() == MARK_QUEEN ? q2 += 3 * add : q2 += add;
    } else if (field.first.getQuadrant() == QUADRANT_FIRST) {
        field.second.getMark() == MARK_QUEEN ? q3 += 3 * add : q3 += add;
    } else {
        field.second.getMark() == MARK_QUEEN ? q4 += 3 * add : q4 += add;
    }
}

int CStaticBoardEvaluator::evaluatePawn(const map<TCoord, CChessman> &fields, const pair<TCoord, CChessman> &field,
                                        bool white) {
    int pawn_value = 0;
    pawn_value += white ? TABLE_PAWN_WHITE.at(field.first.toString()) : TABLE_PAWN_BLACK.at(field.first.toString());
    bool isolated = true;
    //check the surrounding of pawn for other friendly pawns
    for (int i = -1; i < 2; i++) {
        for (int j = -1; j < 2; j++) {
            if ((i != 0 || j != 0) && field.first.row + i >= ROW_MAX && field.first.row + i <= ROW_MAX &&
                field.first.column + j >= COLUMN_MIN && field.first.column + j <= COLUMN_MAX) {
                char r = field.first.row;
                char c = field.first.column;
                if (i == -1) {
                    r--;
                } else if (i == 1) {
                    r++;
                }
                if (j == -1) {
                    c--;
                } else if (j == 1) {
                    c++;
                }
                TCoord target_coord{c, r};
                if (fields.at(target_coord).getMark() == MARK_PAWN && fields.at(target_coord).isWhite() == white) {
                    isolated = false;
                    if (target_coord.column != field.first.column && target_coord.row != field.first.row) {
                        pawn_value += BONUS_PAWN_NEXT_TO_PAWN;
                    }
                }
            }
        }
    }
    if (isolated) {
        pawn_value += PENALTY_PAWN_ISOLATED;
    }
    char row = field.first.row;
    row++;
    if (row <= ROW_MAX && fields.at(TCoord{field.first.column, row}).getMark() == MARK_EMPTY) {
        pawn_value += BONUS_PAWN_FREE_FIELD;
    }
    //test if pawn has free way to end for promotion
    bool free_column = true;
    while (true) {
        if (row > ROW_MAX) {
            break;
        }
        if (fields.at(TCoord{field.first.column, row}).getMark() != MARK_EMPTY) {
            free_column = false;
            break;
        }
        row++;
    }
    if (free_column) {
        pawn_value += BONUS_PAWN_FREE_COLUMN;
    }
    return pawn_value;
}

int CStaticBoardEvaluator::evaluateBishop(const map<TCoord, CChessman> &fields, const pair<TCoord, CChessman> &field,
                                          bool white) {
    int bishop_value = 0;
    bishop_value += white ? TABLE_BISHOP_WHITE.at(field.first.toString()) : TABLE_BISHOP_BLACK.at(
            field.first.toString());
    //for each direction calculate for how many fields he is able to get to
    char column = field.first.column, row = field.first.row;
    while (true) {
        column--;
        row--;
        if (column < COLUMN_MIN || row < ROW_MIN || fields.at(TCoord{column, row}).getMark() != MARK_EMPTY) {
            break;
        } else {
            bishop_value += BONUS_BISHOP_MOBILITY;
        }
    }
    column = field.first.column;
    row = field.first.row;
    while (true) {
        column++;
        row--;
        if (column > COLUMN_MAX || row < ROW_MIN || fields.at(TCoord{column, row}).getMark() != MARK_EMPTY) {
            break;
        } else {
            bishop_value += BONUS_BISHOP_MOBILITY;
        }
    }
    column = field.first.column;
    row = field.first.row;
    while (true) {
        column--;
        row++;
        if (column < COLUMN_MIN || row > ROW_MAX || fields.at(TCoord{column, row}).getMark() != MARK_EMPTY) {
            break;
        } else {
            bishop_value += BONUS_BISHOP_MOBILITY;
        }
    }
    column = field.first.column;
    row = field.first.row;
    while (true) {
        column++;
        row++;
        if (column > COLUMN_MAX || row > ROW_MAX || fields.at(TCoord{column, row}).getMark() != MARK_EMPTY) {
            break;
        } else {
            bishop_value += BONUS_BISHOP_MOBILITY;
        }
    }
    return bishop_value;
}

int CStaticBoardEvaluator::evaluateRook(const map<TCoord, CChessman> &fields, const pair<TCoord, CChessman> &field,
                                        bool white) {
    int rook_value = 0;
    bool free_column = true;
    bool enemy_column = true;
    //for each directions calculate for how many fields he is able to get to
    //also while moving on column test if the column is open or at least semiopen
    char column = field.first.column, row = field.first.row;
    while (true) {
        column--;
        if (column < COLUMN_MIN || fields.at(TCoord{column, row}).getMark() != MARK_EMPTY) {
            break;
        } else {
            rook_value += BONUS_ROOK_MOBILITY;
        }
    }
    column = field.first.column;
    while (true) {
        column++;
        if (column > COLUMN_MAX || fields.at(TCoord{column, row}).getMark() != MARK_EMPTY) {
            break;
        } else {
            rook_value += BONUS_ROOK_MOBILITY;
        }
    }
    column = field.first.column;
    bool bonus = true;
    while (true) {
        row--;
        if (row < ROW_MIN) {
            break;
        } else if (((fields.at(TCoord{column, row}).getMark() != MARK_EMPTY) || !bonus) && !white) {
            bonus = false;
            if (fields.at(TCoord{column, row}).getMark() == MARK_PAWN &&
                fields.at(TCoord{column, row}).isWhite() == white) {
                free_column = false;
                enemy_column = false;
            } else if (fields.at(TCoord{column, row}).getMark() == MARK_PAWN &&
                       fields.at(TCoord{column, row}).isWhite() != white) {
                free_column = false;
            }
        } else if (bonus) {
            rook_value += BONUS_ROOK_MOBILITY;
        }
    }
    row = field.first.row;
    bonus = true;
    while (true) {
        row++;
        if (row > ROW_MAX) {
            break;
        } else if (((fields.at(TCoord{column, row}).getMark() != MARK_EMPTY) || !bonus) && white) {
            bonus = false;
            if (fields.at(TCoord{column, row}).getMark() == MARK_PAWN &&
                fields.at(TCoord{column, row}).isWhite() == white) {
                free_column = false;
                enemy_column = false;
            } else if (fields.at(TCoord{column, row}).getMark() == MARK_PAWN &&
                       fields.at(TCoord{column, row}).isWhite() != white) {
                free_column = false;
            }
        } else if (bonus) {
            rook_value += BONUS_ROOK_MOBILITY;
        }
    }
    if (free_column) {
        rook_value += BONUS_ROOK_COLUMN_OPEN;
    } else if (enemy_column) {
        rook_value += BONUS_ROOK_COLUMN_SEMIOPEN;
    }
    return rook_value;
}

int CStaticBoardEvaluator::evaluateQueen(const map<TCoord, CChessman> &fields, const pair<TCoord, CChessman> &field,
                                         bool white) {
    int queen_value = 0;
    //test queens mobility like for bishop and rook
    char column = field.first.column, row = field.first.row;
    while (true) {
        column--;
        row--;
        if (column < COLUMN_MIN || row < ROW_MIN || fields.at(TCoord{column, row}).getMark() != MARK_EMPTY) {
            break;
        } else {
            queen_value += BONUS_QUEEN_MOBILITY;
        }
    }
    column = field.first.column;
    row = field.first.row;
    while (true) {
        column++;
        row--;
        if (column > COLUMN_MAX || row < ROW_MIN || fields.at(TCoord{column, row}).getMark() != MARK_EMPTY) {
            break;
        } else {
            queen_value += BONUS_QUEEN_MOBILITY;
        }
    }
    column = field.first.column;
    row = field.first.row;
    while (true) {
        column--;
        row++;
        if (column < COLUMN_MIN || row > ROW_MAX || fields.at(TCoord{column, row}).getMark() != MARK_EMPTY) {
            break;
        } else {
            queen_value += BONUS_QUEEN_MOBILITY;
        }
    }
    column = field.first.column;
    row = field.first.row;
    while (true) {
        column++;
        row++;
        if (column > COLUMN_MAX || row > ROW_MAX || fields.at(TCoord{column, row}).getMark() != MARK_EMPTY) {
            break;
        } else {
            queen_value += BONUS_QUEEN_MOBILITY;
        }
    }
    column = field.first.column;
    row = field.first.row;
    while (true) {
        column--;
        if (column < COLUMN_MIN || fields.at(TCoord{column, row}).getMark() != MARK_EMPTY) {
            break;
        } else {
            queen_value += BONUS_QUEEN_MOBILITY;
        }
    }
    column = field.first.column;
    while (true) {
        column++;
        if (column > COLUMN_MAX || fields.at(TCoord{column, row}).getMark() != MARK_EMPTY) {
            break;
        } else {
            queen_value += BONUS_QUEEN_MOBILITY;
        }
    }
    column = field.first.column;
    row = field.first.row;
    while (true) {
        row--;
        if (row < ROW_MIN || fields.at(TCoord{column, row}).getMark() != MARK_EMPTY) {
            break;
        } else {
            queen_value += BONUS_QUEEN_MOBILITY;
        }
    }
    row = field.first.row;
    while (true) {
        row++;
        if (row > ROW_MAX || fields.at(TCoord{column, row}).getMark() != MARK_EMPTY) {
            break;
        } else {
            queen_value += BONUS_QUEEN_MOBILITY;
        }
    }
    for (const auto &field2 : fields) {
        if (field2.second.getMark() == MARK_KING && field2.second.isWhite() != white) {
            queen_value += (8 - abs(field2.first.row - field.first.row)) * BONUS_QUEEN_NEAR_ENEMY_KING;
            queen_value += (8 - abs(field2.first.column - field.first.column)) * BONUS_QUEEN_NEAR_ENEMY_KING;
        }
    }
    return queen_value;
}

int CStaticBoardEvaluator::evaluateKing(const map<TCoord, CChessman> &fields, const pair<TCoord, CChessman> &field,
                                        bool white) {
    int king_value = 0;
    if (field.second.isMoved()) {
        king_value += PENALTY_KING_NO_CASTLING;
        return king_value;
    }
    if (white) {
        if (fields.at(TCoord{COLUMN_A,ROW_1}).isMoved()) {
            king_value += PENALTY_KING_NO_QUEENCASTLING;
        }
        if (fields.at(TCoord{COLUMN_H,ROW_1}).isMoved()) {
            king_value += PENALTY_KING_NO_KINGCASTLING;
        }
    } else {
        if (fields.at(TCoord{COLUMN_A,ROW_8}).isMoved()) {
            king_value += PENALTY_KING_NO_QUEENCASTLING;
        }
        if (fields.at(TCoord{COLUMN_A,ROW_8}).isMoved()) {
            king_value += PENALTY_KING_NO_KINGCASTLING;
        }
    }
    return king_value;
}