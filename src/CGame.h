/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 3. 5. 2020
 */

#pragma once

#include <iostream>
#include <memory>
#include <fstream>
#include "CBoard.h"
#include "TCoord.h"
#include "CPlayer.h"
#include "CPerson.h"
#include "CArtificial.h"
#include "constants.h"

class CGame {
    bool m_Running;
    bool m_Saved;
    std::size_t m_MoveCount;
    CBoard m_Board;
    std::unique_ptr<CPlayer> m_Player1;
    std::unique_ptr<CPlayer> m_Player2;

public:
    CGame();

    CGame(const CGame &src);

    CGame &operator=(const CGame &src);

    ~CGame() = default;

    /**
     * Starts completely new game with given players.
     * @param p1 pointer to player 1
     * @param p2 pointer to player 2
     */
    void startNew(const std::unique_ptr<CPlayer> &p1, const std::unique_ptr<CPlayer> &p2);

    /**
     * Saves game to file
     * @param ofs output filestream of the savefile where the game will be saved
     * @return true if saving was successful and the ofs is OK
     */
    bool save(std::ofstream &ofs);

    /**
     * Loads a game from file  with checking that file is not corrupted.
     * @param ifs input filestream of the savefile from which the game is loaded
     * @param example is set for CArtificial if the file is example
     * @return true if loading was successful and the ifs is OK
     */
    bool load(std::ifstream &ifs, bool example);

    /**
     * Validate the move in m_Board. If the move is OK, then tests if some type of ending occurs and increase m_MoveCount
     * @param parameters defines the move by coordinates
     * @param ending output parameter to store ending
     * @return true if move is valid
     */
    bool commitMove(const std::string &parameters, int &ending);

    /** @return m_MoveCount */
    size_t getMoveCount() const;

    /** @return true if game is running */
    bool isRunning() const;

    /** @param mRunning defines what value is to be m_Running set */
    void setRunning(bool mRunning);

    /** @return true if game is saved */
    bool isSaved() const;

    /** @return m_Board that is assigned to game */
    const CBoard &getBoard() const;

    /**
     * @param white defines which player will be returned
     * @return unique_ptr to chosen player
     */
    const std::unique_ptr<CPlayer> &getPlayer(bool white) const;
};


