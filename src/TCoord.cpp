/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 4. 5. 2020
 */


#include "TCoord.h"

using namespace std;

TCoord::TCoord(const char &c, const char &r) : column(c), row(r) {
    if (((c < COLUMN_MIN || c > COLUMN_MAX) && c != RETURN_NO_CHECK[0]) || ((r < ROW_MIN || r > ROW_MAX) && r != '0')) {
        throw invalid_argument(INVALID_ARGUMENT_TCOORD);
    }
}

TCoord::TCoord(const char *src) {
    if (strlen(src) < 2 || ((src[0] < COLUMN_MIN || src[0] > COLUMN_MAX) && src[0] != RETURN_NO_CHECK[0]) ||
        ((src[1] < ROW_MIN || src[1] > ROW_MAX) && src[1] != RETURN_NO_CHECK[0])) {
        throw invalid_argument(INVALID_ARGUMENT_TCOORD);
    }
    column = src[0];
    row = src[1];
}

std::string TCoord::toString() const {
    string out;
    out += column;
    out += row;
    return out;
}


int TCoord::getQuadrant() const {
    if (column < COLUMN_E) {
        return row < ROW_5 ? QUADRANT_FIRST : QUADRANT_SECOND;
    } else {
        return row < ROW_5 ? QUADRANT_THIRD : QUADRANT_FOURTH;
    }
}

bool operator==(const TCoord &lhs, const TCoord &rhs) {
    return lhs.column == rhs.column && lhs.row == rhs.row;
}

bool operator!=(const TCoord &lhs, const TCoord &rhs) {
    return !(lhs == rhs);
}

bool operator<(const TCoord &lhs, const TCoord &rhs) {
    return (lhs.row > rhs.row || (lhs.row == rhs.row && lhs.column < rhs.column));
}