/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 4. 5. 2020
 */

#pragma once

#include <string>
#include <cstring>
#include <stdexcept>
#include "constants.h"

struct TCoord {
    char column;
    char row;

    /** @throws std::invalid_argument exception if the source string doesn't have at least 2 characters or if the characters aren't valid coordinates */
    TCoord(const char &c, const char &r);

    /** @throws std::invalid_argument exception if the source string doesn't have at least 2 characters or if the characters aren't valid coordinates */
    TCoord(const char *src);

    /** @return coordinates in string format*/
    std::string toString() const;

    /** @return number of quadrant in chess board where the coordinates belong */
    int getQuadrant() const;

    /**
     * @param lhs left coordinate
     * @param rhs right coordinate
     * @return true if columns and rows are equal
     */
    friend bool operator==(const TCoord &lhs, const TCoord &rhs);

    /**
     * @param lhs left coordinate
     * @param rhs right coordinate
     * @return true if columns and rows are not equal
     */
    friend bool operator!=(const TCoord &lhs, const TCoord &rhs);

    /**
     * Compares coordinates primarily by row in descending order. If the rows are same then compares by column in ascending order.
     * @param lhs left coordinate
     * @param rhs right coordinate
     * @return true if lhs is lower than rhs
     */
    friend bool operator<(const TCoord &lhs, const TCoord &rhs);
};