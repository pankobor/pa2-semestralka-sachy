/**
 * @author David Bernhauer
 * source: https://gitlab.fit.cvut.cz/bernhdav/pa2-minesweeper/blob/master/src/CCommand.h
 */

#pragma once

#include <string>
#include <functional>
#include "CPlayer.h"
#include "CPerson.h"
#include "CArtificial.h"
#include "CInterface.h"
#include "CGame.h"

class CCommand {
    std::string m_Help;
    std::function<bool(const CInterface &, CGame &, std::string &)> m_Execute;
public:
    CCommand(std::string mHelp, std::function<bool(const CInterface &, CGame &, std::string &)> mExecute) noexcept;

    /** @return m_Help */
    const std::string &getHelp() const;

    /** Execute the m_Execute
     * @return true if the m_Execute was successful
     */
    bool execute(const CInterface &interface, CGame &game, std::string &params);
};


