/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 7. 5. 2020
 */

#include "chessmen.h"

using namespace std;


CChessman empty(bool) {
    return CChessman{
            false,
            MARK_EMPTY,
            [](const TCoord &, const TCoord &) {
                return vector<TCoord>{};
            },
            true
    };
}

CChessman pawn(bool white) {
    return CChessman{
            white,
            MARK_PAWN,
            [white](const TCoord &from, const TCoord &to) {
                if ((to.column == from.column - 1 || to.column == from.column || to.column == from.column + 1) &&
                    ((white && to.row == from.row + 1) || (!white && to.row == from.row - 1))) {
                    return vector<TCoord>{from, to};
                } else if (to.column == from.column &&
                           ((white && from.row == ROW_2 && to.row == ROW_4) ||
                            (!white && from.row == ROW_7 && to.row == ROW_5))) {
                    char x = to.row;
                    white ? x-- : x++;
                    return vector<TCoord>{from, TCoord{from.column, x}, to};
                } else {
                    return vector<TCoord>{};
                }
            }
    };
}

CChessman rook(bool white) {
    return CChessman{
            white,
            MARK_ROOK,
            [](const TCoord &from, const TCoord &to) {
                if (to.row == from.row && to.column != from.column) {
                    vector<TCoord> path;
                    if (from.column < to.column) {
                        for (char c = from.column; c <= to.column; c++) {
                            path.emplace_back(TCoord{c, from.row});
                        }
                    } else {
                        for (char c = from.column; c >= to.column; c--) {
                            path.emplace_back(TCoord{c, from.row});
                        }
                    }
                    return path;
                } else if (to.column == from.column && to.row != from.row) {
                    vector<TCoord> path;
                    if (from.row < to.row) {
                        for (char i = from.row; i <= to.row; i++) {
                            path.emplace_back(TCoord{from.column, i});
                        }
                    } else {
                        for (char i = from.row; i >= to.row; i--) {
                            path.emplace_back(TCoord{from.column, i});
                        }
                    }
                    return path;
                } else {
                    return vector<TCoord>{};
                }
            }
    };
}

CChessman knight(bool white) {
    return CChessman{
            white,
            MARK_KNIGHT,
            [](const TCoord &from, const TCoord &to) {
                if (((to.column == from.column - 2 || to.column == from.column + 2) &&
                     (to.row == from.row - 1 || to.row == from.row + 1)) ||
                    ((to.column == from.column - 1 || to.column == from.column + 1) &&
                     (to.row == from.row - 2 || to.row == from.row + 2))) {
                    //knight is jumping over fields, so his path consists only of from and to fields
                    return vector<TCoord>{from, to};
                } else {
                    return vector<TCoord>{};
                }
            }
    };
}

CChessman bishop(bool white) {
    return CChessman{
            white,
            MARK_BISHOP,
            [](const TCoord &from, const TCoord &to) {
                if (abs(to.row - from.row) == abs(to.column - from.column) && to.row != from.row) {
                    char c = from.column;
                    char i = from.row;
                    vector<TCoord> path;
                    for (int j = 0; j <= (abs(to.row - from.row)); j++) {
                        path.emplace_back(TCoord{c, i});
                        if (from.column < to.column) {
                            c++;
                        } else {
                            c--;
                        }
                        if (from.row < to.row) {
                            i++;
                        } else {
                            i--;
                        }
                    }
                    return path;
                } else {
                    return vector<TCoord>{};
                }
            }
    };
}

CChessman queen(bool white) {
    return CChessman{
            white,
            MARK_QUEEN,
            [](const TCoord &from, const TCoord &to) {
                //moves like rook
                if (to.row == from.row && to.column != from.column) {
                    vector<TCoord> path;
                    if (from.column < to.column) {
                        for (char c = from.column; c <= to.column; c++) {
                            path.emplace_back(TCoord{c, from.row});
                        }
                    } else {
                        for (char c = from.column; c >= to.column; c--) {
                            path.emplace_back(TCoord{c, from.row});
                        }
                    }
                    return path;
                } else if (to.column == from.column && to.row != from.row) {
                    vector<TCoord> path;
                    if (from.row < to.row) {
                        for (char i = from.row; i <= to.row; i++) {
                            path.emplace_back(TCoord{from.column, i});
                        }
                    } else {
                        for (char i = from.row; i >= to.row; i--) {
                            path.emplace_back(TCoord{from.column, i});
                        }
                    }
                    return path;
                } else if (abs(to.row - from.row) == abs(to.column - from.column) && to.row != from.row) {
                    //moves like bishop
                    char c = from.column;
                    char i = from.row;
                    vector<TCoord> path;
                    for (int j = 0; j <= (abs(to.row - from.row)); j++) {
                        path.emplace_back(TCoord{c, i});
                        if (from.column < to.column) {
                            c++;
                        } else {
                            c--;
                        }
                        if (from.row < to.row) {
                            i++;
                        } else {
                            i--;
                        }
                    }
                    return path;
                } else {
                    return vector<TCoord>{};
                }
            }
    };
}

CChessman king(bool white) {
    return CChessman{
            white,
            MARK_KING,
            [](const TCoord &from, const TCoord &to) {
                if ((to.column == from.column - 1 || to.column == from.column || to.column == from.column + 1) &&
                    (to.row == from.row - 1 || to.row == from.row || to.row == from.row + 1) &&
                    (to.row != from.row || to.column != from.column)) {
                    return vector<TCoord>{from, to};
                } else {
                    return vector<TCoord>{};
                }
            }
    };
}
