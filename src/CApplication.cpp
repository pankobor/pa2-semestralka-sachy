/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 3. 5. 2020
 * inspired by: https://gitlab.fit.cvut.cz/bernhdav/pa2-minesweeper/blob/master/src/CApplication.cpp
 */

#include "CApplication.h"

using namespace std;

CApplication::CApplication(CInterface interface) : m_Interface(interface) {
    m_Commands.emplace(COMMAND_NEW, newCommand());
    m_Commands.emplace(COMMAND_LOAD, loadCommand());
    m_Commands.emplace(COMMAND_SAVE, saveCommand());
    m_Commands.emplace(COMMAND_MOVE, moveCommand());
    m_Commands.emplace(COMMAND_SURRENDER, surrenderCommand());
    m_Commands.emplace(COMMAND_HELP, helpCommand(m_Commands));
    m_Commands.emplace(COMMAND_QUIT, quitCommand());
    interface.print(TEXT_INTRO);
}

int CApplication::Run() {
    while (true) {
        string parameters;
        string command = m_Interface.promptCommand(
                [this](const string &command) {
                    return m_Commands.find(command) != m_Commands.end();
                }, m_Game, parameters);
        auto it = m_Commands.find(command);
        if (it == m_Commands.end()) {
            throw logic_error(ERROR_INVALID_INPUT);
        }
        if (!it->second.execute(m_Interface, m_Game, parameters)) {
            return 0;
        }
    }
    return 57;
}

