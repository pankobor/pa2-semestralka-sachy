/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 9. 5. 2020
 */

#include "CPerson.h"

using namespace std;

bool CPerson::save(ofstream &ofs) const {
    ofs << MARK_PLAYER;
    return ofs.is_open() && !ofs.bad() && !ofs.fail();
}

unique_ptr<CPlayer> CPerson::clone() const {
    return make_unique<CPerson>(*this);
}

string CPerson::requestMove(istream &in, ostream &out, const CBoard &board, string &params) {
    (void) board; //board is not used, because real person can see the board in terminal
    string command;
    if (!(in >> command)) {
        out << ERROR_INVALID_INPUT << endl;
    }
    //skip the blank characters before command parameters
    while (in.peek() == ' ') {
        in.get();
    }
    if (in.peek() != '\n') {
        in >> params;
    }
    return command;
}