/**
 * @author David Bernhauer
 * source: https://gitlab.fit.cvut.cz/bernhdav/pa2-minesweeper/blob/master/src/CApplication.h
 */

#pragma once

#include <string>
#include <map>
#include <memory>
#include "CInterface.h"
#include "CCommand.h"
#include "CGame.h"
#include "CPlayer.h"
#include "commands.h"

class CApplication {
    CInterface m_Interface;
    std::map<std::string, CCommand> m_Commands;
    CGame m_Game;
public:
    explicit CApplication(CInterface interface);

    /**Copy constructor and assignment deleted, because copy of whole application is of no use in our program.*/
    CApplication(const CApplication &) = delete;

    CApplication &operator=(const CApplication &) = delete;

    ~CApplication() = default;

    /**
     * Basic game loop. Requests and execute commands from user.
     * @return 0 if ended properly
     * @throws logic error if invalid input wasn't handled correctly in promptCommand
     */
    int Run();
};

