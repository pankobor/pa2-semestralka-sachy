/**
 * @author David Bernhauer
 * source: https://gitlab.fit.cvut.cz/bernhdav/pa2-minesweeper/blob/master/src/CCommand.src
 */

#include "CCommand.h"

using namespace std;

CCommand::CCommand(string mHelp, function<bool(const CInterface &, CGame &, string &parameters)> mExecute) noexcept
        : m_Help(move(mHelp)), m_Execute(move(mExecute)) {}

const std::string &CCommand::getHelp() const {
    return m_Help;
}

bool CCommand::execute(const CInterface &interface, CGame &game, string &params) {
    return m_Execute(interface, game, params);
}