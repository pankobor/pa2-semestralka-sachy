/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 6. 5. 2020
 */

#include "CChessman.h"

using namespace std;

CChessman::CChessman(bool mWhite, char mMark,
                     function<vector<TCoord>(const TCoord &from, const TCoord &to)> mPath, bool mMoved) noexcept
        : m_Mark(mMark), m_Path(move(mPath)), m_White(mWhite), m_Moved(mMoved) {}

vector<TCoord> CChessman::getPath(const TCoord &from, const TCoord &to) const {
    return m_Path(from, to);
}

bool CChessman::isWhite() const {
    return m_White;
}

char CChessman::getMark() const {
    return m_Mark;
}

bool CChessman::isMoved() const {
    return m_Moved;
}

void CChessman::setMoved(bool mMoved) {
    m_Moved = mMoved;
}

std::ostream &operator<<(ostream &out, const CChessman &chessman) {
    if (chessman.m_White) {
        out << static_cast<char>(toupper(chessman.m_Mark));
    } else {
        out << chessman.m_Mark;
    }
    return out;
}

bool operator==(const CChessman &lhs, const CChessman &rhs) {
    return rhs.m_Mark == lhs.m_Mark && rhs.m_Moved == lhs.m_Moved && rhs.m_White == lhs.m_White;
}

bool operator!=(const CChessman &lhs, const CChessman &rhs) {
    return !(rhs == lhs);
}
