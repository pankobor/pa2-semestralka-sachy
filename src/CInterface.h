/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 3. 5. 2020
 * inspired by: https://gitlab.fit.cvut.cz/bernhdav/pa2-minesweeper/blob/master/src/CInterface.h
 */

#pragma once

#include <iostream>
#include <string>
#include <functional>
#include <limits>
#include <map>
#include "CGame.h"
#include "constants.h"

class CInterface {
    std::istream &m_In;
    std::ostream &m_Out;

    /**
     * Clears the content of m_In
     * @author David Bernhauer
     * source: https://gitlab.fit.cvut.cz/bernhdav/pa2-minesweeper/blob/master/src/CInterface.h
     */
    void clearLine() const;

public:
    explicit CInterface(std::istream &mIn = std::cin, std::ostream &mOut = std::cout);

    CInterface(const CInterface &src);

    /** copy assigment deleted because assigment of streams is implicitly deleted */
    CInterface &operator=(CInterface &src) = delete;

    ~CInterface() = default;

    /**
     * Prompts command and parameters from user. Has different interface for while the game us running.
     * @param valid used to check if command from user is valid
     * @param game
     * @param params output parameter for parameters of command
     * @return command from user or AI
     */
    std::string
    promptCommand(const std::function<bool(const std::string &)> &valid, const CGame &game, std::string &params) const;

    /**
     * @param text with description of situation and possible options, demanding one of the given options from user
     * @param choices possible choices that user can enter, if the user enters something else, it will prompt text again
     * @return choice that user made
     */
    char promptWithOptions(const std::string &text, const std::string &choices) const;

    /**
     * Prints a command and help to m_Out
     * @param command string containing command
     * @param help string containing help for used command
     */
    void printHelp(const std::string &command, const std::string &help) const;

    /** @param message is printed to m_Out */
    void print(const std::string &message) const;

    /** @param game used to access a board which is printed to m_Out  */
    void printBoard(const CGame &game) const;

    /**
     * Prints message about ending or event that occurred after players move.
     * @param ending defines what event occurred
     * @param game is used to get move count of a game
     */
    void printEvent(int ending, const CGame &game) const;
};


