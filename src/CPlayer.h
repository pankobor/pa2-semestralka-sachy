/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 4. 5. 2020
 */

#pragma once

#include <memory>
#include <string>
#include <iostream>
#include <fstream>
#include "CBoard.h"

class CPlayer {
public:
    CPlayer() = default;

    CPlayer(const CPlayer &src) = default;

    /**Copy assigment deleted, because CPlayer is pure virtual class*/
    CPlayer & operator = (const CPlayer &src) = delete;

    virtual ~CPlayer() = default;

    virtual bool save(std::ofstream &)const = 0;

    virtual void endMessage(std::ostream &,bool) const = 0;

    virtual std::unique_ptr<CPlayer> clone() const = 0;

    virtual std::string requestMove(std::istream &,std::ostream &, const CBoard &, std::string &) = 0;
};


