/**
 * @author Boris Pankovčin <pankobor@fit.cvut.cz>
 * @date 4. 5. 2020
 */

#pragma once

#include <map>
#include <vector>
#include <fstream>
#include "TCoord.h"
#include "chessmen.h"
#include "CChessman.h"
#include "constants.h"


class CBoard {
    std::map<TCoord, CChessman> m_Fields;

    /** Test if move meets all the conditions for castling and if yes updates the m_Fields
     * @param from defines the source field with moving chessman
     * @param to defines the target field
     * @return true if castling was successful
     */
    bool tryCastling(const TCoord &from, const TCoord &to);

    /** Test if move meets all the conditions for pawn promotion and if yes updates the m_Fields
     * @param from defines the source field with moving chessman
     * @param to defines the target field
     * @param promoted_to defines to which type of chessman should be the pawn promoted
     * @return true if promotion was successful
     */
    bool tryPromotion(const TCoord &from, const TCoord &to, const char &promoted_to);

    /** Test if move meets all the conditions for en passant and if yes updates the m_Fields
     * @param from defines the source field with moving chessman
     * @param to defines the target field
     * @return true if en passant was successful
     */
    bool tryEnPassant(const TCoord &from, const TCoord &to);

    /**
     * Test if the target field is endangered by any enemy chessman.
     * @param target_field defines which field is tested
     * @param white defines the enemy color
     * @return position of first enemy chessman that threatens the target field
     */
    TCoord testCheck(const TCoord &target_field, bool white) const;

    /**
     * @param path vector of coordinates through which goes the chessman
     * @return true if the path is valid, e.g. no chessman is blocking the path, no check for king etc.
     */
    bool testPath(const std::vector<TCoord> &path) const;

    /**
     * @param white defines which king will be tested
     * @return true if king is in check and there is no possible move to avoid it
     */
    bool testCheckmate(bool white);

    /**
     * @param white defines which player is on the move
     * @return true if king is not in check but the player has no possible move
     */
    bool testStalemate(bool white) const;

    /** Sets all pawns in rows 4 and 5 moved */
    void setPawnsMoved();

public:
    CBoard();

    /** Reset m_Fields to it's default state. */
    void reset();

    /** @return m_Fields */
    const std::map<TCoord, CChessman> &getFields() const;

    /** @param out defines the output stream where the board will be printed */
    void print(std::ostream &out) const;

    /** @param ofs defines output filestream where the board will be saved */
    bool save(std::ofstream &ofs) const;

    /** Load board from savefile and test if the board is valid
     * @param ifs input filestream from which the board will be loaded
     * @param move_count to test that king is not in check
     * @return true if loading was successful
     */
    bool load(std::ifstream &ifs, int &move_count);

    /**
     * Test that move is correct and doesn't violate any chess rule. If the move is OK, then updates the m_Fields.
     * @param from coordinates of chessman which will make the move
     * @param to coordinates of target field
     * @param promoted_to mark of new chessman used in pawn promotion
     * @param white indicates the color of player which is making the move
     * @return true if the move is fine
     */
    bool validateMove(const TCoord &from, const TCoord &to, char promoted_to, bool white);

    /** Undo a move without control and also comparing pawns at rows 4, 5 with original fields
     * @param from defines move coordinates
     * @param to defines move coordinates
     * @param original_fields representing the whole original board as a specimen
     */
    void undoMove(const TCoord &from, const TCoord &to, const std::map<TCoord, CChessman> &original_fields);

    /**
     * @param white defines which king will be tested
     * @return true if king is in check
     */
    bool testCheck(bool white) const;

    /**
     * @param white defines which player will be tested
     * @return ending depending on whether player is in check/checkmate/stalemate or nothing of those
     */
    int testEnd(bool white);
};


