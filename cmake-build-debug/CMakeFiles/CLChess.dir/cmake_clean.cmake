file(REMOVE_RECURSE
  "CLChess.exe"
  "CLChess.pdb"
  "CMakeFiles/CLChess.dir/src/CApplication.cpp.o"
  "CMakeFiles/CLChess.dir/src/CArtificial.cpp.o"
  "CMakeFiles/CLChess.dir/src/CBoard.cpp.o"
  "CMakeFiles/CLChess.dir/src/CChessman.cpp.o"
  "CMakeFiles/CLChess.dir/src/CCommand.cpp.o"
  "CMakeFiles/CLChess.dir/src/CGame.cpp.o"
  "CMakeFiles/CLChess.dir/src/CInterface.cpp.o"
  "CMakeFiles/CLChess.dir/src/CPerson.cpp.o"
  "CMakeFiles/CLChess.dir/src/CStaticBoardEvaluator.cpp.o"
  "CMakeFiles/CLChess.dir/src/TCoord.cpp.o"
  "CMakeFiles/CLChess.dir/src/chessmen.cpp.o"
  "CMakeFiles/CLChess.dir/src/commands.cpp.o"
  "CMakeFiles/CLChess.dir/src/main.cpp.o"
  "CMakeFiles/CLChess.dir/test.cpp.o"
  "libCLChess.dll.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/CLChess.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
