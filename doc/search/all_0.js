var searchData=
[
  ['bishop',['bishop',['../chessmen_8cpp.html#a4ea50ba9459e59e06e22600c5ddba494',1,'bishop(bool white):&#160;chessmen.cpp'],['../chessmen_8h.html#a4ea50ba9459e59e06e22600c5ddba494',1,'bishop(bool white):&#160;chessmen.cpp']]],
  ['bonus_5fbishop_5fmobility',['BONUS_BISHOP_MOBILITY',['../constants_8h.html#a5093a7cf1d48be9f5b3f6342159adfe3',1,'constants.h']]],
  ['bonus_5fbishop_5fmore',['BONUS_BISHOP_MORE',['../constants_8h.html#a022150656fd53afd9dbe0c8dac760ac6',1,'constants.h']]],
  ['bonus_5fcheck',['BONUS_CHECK',['../constants_8h.html#a7ac7d9b75cbf513dcb1e1b1e4b2380f6',1,'constants.h']]],
  ['bonus_5fking_5fquadrant_5fdominance',['BONUS_KING_QUADRANT_DOMINANCE',['../constants_8h.html#a123a32634a8b9080ac5f9780aa73834b',1,'constants.h']]],
  ['bonus_5fpawn_5ffree_5fcolumn',['BONUS_PAWN_FREE_COLUMN',['../constants_8h.html#af23c774a995bde7c73ffabc49eb24f3b',1,'constants.h']]],
  ['bonus_5fpawn_5ffree_5ffield',['BONUS_PAWN_FREE_FIELD',['../constants_8h.html#abd21f06baebae9e6ca57e8025fb4612f',1,'constants.h']]],
  ['bonus_5fpawn_5fnext_5fto_5fpawn',['BONUS_PAWN_NEXT_TO_PAWN',['../constants_8h.html#af59b3848e8dbfd6efd701323aee90f6e',1,'constants.h']]],
  ['bonus_5fqueen_5fmobility',['BONUS_QUEEN_MOBILITY',['../constants_8h.html#a813d64443d5a48167c2679a2722c4f18',1,'constants.h']]],
  ['bonus_5fqueen_5fnear_5fenemy_5fking',['BONUS_QUEEN_NEAR_ENEMY_KING',['../constants_8h.html#a01016083493d79c24888bc3c5f117d92',1,'constants.h']]],
  ['bonus_5frook_5fcolumn_5fopen',['BONUS_ROOK_COLUMN_OPEN',['../constants_8h.html#a5dfb3876d6f81e201fff0331c82ecf24',1,'constants.h']]],
  ['bonus_5frook_5fcolumn_5fsemiopen',['BONUS_ROOK_COLUMN_SEMIOPEN',['../constants_8h.html#aac264b6ad5a39c2cc6cc27e5c2705e9f',1,'constants.h']]],
  ['bonus_5frook_5fdoubled_5fcolumn',['BONUS_ROOK_DOUBLED_COLUMN',['../constants_8h.html#aa21f172e9bbc2cf9eaa6a7c6760fc8f3',1,'constants.h']]],
  ['bonus_5frook_5fdoubled_5frow',['BONUS_ROOK_DOUBLED_ROW',['../constants_8h.html#ae8eab6559dcd1a58d14f18a297247a25',1,'constants.h']]],
  ['bonus_5frook_5fmobility',['BONUS_ROOK_MOBILITY',['../constants_8h.html#aea66656abf5d74dd26f14490af8ed335',1,'constants.h']]],
  ['bonus_5frook_5fseventh_5frank',['BONUS_ROOK_SEVENTH_RANK',['../constants_8h.html#ab3616462dfc95ebb9af1e11a4bbfb8a9',1,'constants.h']]],
  ['bonus_5fstalemate',['BONUS_STALEMATE',['../constants_8h.html#abe29180e7dac66b2eb387320d45053c2',1,'constants.h']]]
];
