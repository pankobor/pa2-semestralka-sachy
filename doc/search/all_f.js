var searchData=
[
  ['readme_2emd',['README.md',['../README_8md.html',1,'']]],
  ['requestmove',['requestMove',['../classCArtificial.html#ac088380b0ec288a4b76e5713faa9bdad',1,'CArtificial::requestMove()'],['../classCPerson.html#aaf489f456f71ae20edc563066ecf6b37',1,'CPerson::requestMove()'],['../classCPlayer.html#ac838d1a7a760a501f1dd08c41185505b',1,'CPlayer::requestMove()']]],
  ['reset',['reset',['../classCBoard.html#afbd4e2cf8f9920321eaaefc8664f944f',1,'CBoard']]],
  ['return_5fno_5fcheck',['RETURN_NO_CHECK',['../constants_8h.html#a335fa2ee7b42a8ca43ee5102c98e37e7',1,'constants.h']]],
  ['rook',['rook',['../chessmen_8cpp.html#a64ebf481f8a99bc8a62006cd3cc5c128',1,'rook(bool white):&#160;chessmen.cpp'],['../chessmen_8h.html#a64ebf481f8a99bc8a62006cd3cc5c128',1,'rook(bool white):&#160;chessmen.cpp']]],
  ['row',['row',['../structTCoord.html#aa4ce490b4b26d87861ec9bacb1a7d8fd',1,'TCoord']]],
  ['row_5f1',['ROW_1',['../constants_8h.html#a59013ae2273bc0d2e4a52ef909a83bdd',1,'constants.h']]],
  ['row_5f2',['ROW_2',['../constants_8h.html#a146ed6b3ca53d4e67b4e60e32a594f5a',1,'constants.h']]],
  ['row_5f3',['ROW_3',['../constants_8h.html#a25c3dcb972324438c8dc3c5c16aeab1e',1,'constants.h']]],
  ['row_5f4',['ROW_4',['../constants_8h.html#aae9c6d6abf5ce60215ef0b23ad98184d',1,'constants.h']]],
  ['row_5f5',['ROW_5',['../constants_8h.html#a5a8c275061ed5247724c2629b924ac8b',1,'constants.h']]],
  ['row_5f6',['ROW_6',['../constants_8h.html#ada3ee0bbf01c5258dc8df9a5be141316',1,'constants.h']]],
  ['row_5f7',['ROW_7',['../constants_8h.html#abca7896a70a398b30571ad78690206c8',1,'constants.h']]],
  ['row_5f8',['ROW_8',['../constants_8h.html#abeb01a7dfc72db562baa99fcc800370d',1,'constants.h']]],
  ['row_5fmax',['ROW_MAX',['../constants_8h.html#a6e43841da884e263f15ecb92740b00af',1,'constants.h']]],
  ['row_5fmin',['ROW_MIN',['../constants_8h.html#a220e3b09916af705552cf9d0576b34e9',1,'constants.h']]],
  ['rows',['ROWS',['../constants_8h.html#a7dfd02e7225a596b5a568ad0cc6d881a',1,'constants.h']]],
  ['run',['Run',['../classCApplication.html#aa5bea3ca596636a50a27101fb3a4da0b',1,'CApplication']]]
];
