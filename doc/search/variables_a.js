var searchData=
[
  ['penalty_5fcheckmate',['PENALTY_CHECKMATE',['../constants_8h.html#ad973d0fb4f52c793cd227cce5b825050',1,'constants.h']]],
  ['penalty_5fking_5fno_5fcastling',['PENALTY_KING_NO_CASTLING',['../constants_8h.html#a85b6ce4766efa8791549e9b230e695dd',1,'constants.h']]],
  ['penalty_5fking_5fno_5fkingcastling',['PENALTY_KING_NO_KINGCASTLING',['../constants_8h.html#aeaa66da78e979205d44a5ee20d1cde0a',1,'constants.h']]],
  ['penalty_5fking_5fno_5fqueencastling',['PENALTY_KING_NO_QUEENCASTLING',['../constants_8h.html#a2462e4bce44c89bb2704f1ab34ca16ab',1,'constants.h']]],
  ['penalty_5fpawn_5fdoubled',['PENALTY_PAWN_DOUBLED',['../constants_8h.html#ac3be87b959152645ebd86a8376a9b701',1,'constants.h']]],
  ['penalty_5fpawn_5fisolated',['PENALTY_PAWN_ISOLATED',['../constants_8h.html#a137b08be796abba21670b8251f341cc9',1,'constants.h']]],
  ['penalty_5fqueen_5fearly_5fout',['PENALTY_QUEEN_EARLY_OUT',['../constants_8h.html#ad829c02a7f834dc7f9e69e7b5d9e979a',1,'constants.h']]],
  ['phase_5fearly',['PHASE_EARLY',['../constants_8h.html#ac47e0e91bbbb046ff3967e57d1012493',1,'constants.h']]],
  ['phase_5fend',['PHASE_END',['../constants_8h.html#aa0e9cb7ae2fa94561eebd25278227975',1,'constants.h']]],
  ['phase_5fmiddle',['PHASE_MIDDLE',['../constants_8h.html#ae69037cae3d5863dfc5d70eb67e1835d',1,'constants.h']]]
];
