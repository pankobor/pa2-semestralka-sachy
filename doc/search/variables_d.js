var searchData=
[
  ['table_5fbishop_5fblack',['TABLE_BISHOP_BLACK',['../constants_8h.html#a3093e23d6209949c07b3930ecdb30937',1,'constants.h']]],
  ['table_5fbishop_5fwhite',['TABLE_BISHOP_WHITE',['../constants_8h.html#a3bbc9ca20d113e86bcc9f44db9e4f5f1',1,'constants.h']]],
  ['table_5fking_5fblack',['TABLE_KING_BLACK',['../constants_8h.html#a91181c8354d92e601f97ae213b73b7e5',1,'constants.h']]],
  ['table_5fking_5fendgame',['TABLE_KING_ENDGAME',['../constants_8h.html#aae86c055d24763035863abd5bdccda2e',1,'constants.h']]],
  ['table_5fking_5fwhite',['TABLE_KING_WHITE',['../constants_8h.html#a4db36a7de2986e0951dfe6741ece5375',1,'constants.h']]],
  ['table_5fknight_5fblack',['TABLE_KNIGHT_BLACK',['../constants_8h.html#a29dd7dae9f97437d74b040bc26552a8e',1,'constants.h']]],
  ['table_5fknight_5fwhite',['TABLE_KNIGHT_WHITE',['../constants_8h.html#acf5cc67a2a8302bc64f9dd4a7dcc2d1d',1,'constants.h']]],
  ['table_5fpawn_5fblack',['TABLE_PAWN_BLACK',['../constants_8h.html#a8b4158862bbce2baaeec5c5257b6be39',1,'constants.h']]],
  ['table_5fpawn_5fwhite',['TABLE_PAWN_WHITE',['../constants_8h.html#a0e30a41b486d5349182b246f192daa12',1,'constants.h']]],
  ['text_5fai_5fmove',['TEXT_AI_MOVE',['../constants_8h.html#ad45c80f79663235b7a069ae6dae3aaef',1,'constants.h']]],
  ['text_5fai_5fthinking',['TEXT_AI_THINKING',['../constants_8h.html#a4cf7f1c88749c1af1fc3ef8512cd2405',1,'constants.h']]],
  ['text_5fintro',['TEXT_INTRO',['../constants_8h.html#a424f41d46fbd231dc9bb61e8f7308ef9',1,'constants.h']]]
];
