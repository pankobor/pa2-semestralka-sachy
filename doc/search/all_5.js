var searchData=
[
  ['gamemode_5fmultiplayer',['GAMEMODE_MULTIPLAYER',['../constants_8h.html#adf9a0c1eb53948ac12b814dcbe872186',1,'constants.h']]],
  ['gamemode_5fsingleplayer',['GAMEMODE_SINGLEPLAYER',['../constants_8h.html#a8cc1b7f5a8980d3bf912fd4c073623c0',1,'constants.h']]],
  ['gamephase',['gamePhase',['../classCStaticBoardEvaluator.html#a7389fff4d501c5c709bcfd65b3dd6ef3',1,'CStaticBoardEvaluator']]],
  ['getboard',['getBoard',['../classCGame.html#a29756402f17c82ad645928a6ab9e8063',1,'CGame']]],
  ['getfields',['getFields',['../classCBoard.html#a04faa6e6daa2e4d47a336ac2089d9c1a',1,'CBoard']]],
  ['gethelp',['getHelp',['../classCCommand.html#adb6d3f6da359a8a95b68a319d22eb26e',1,'CCommand']]],
  ['getmark',['getMark',['../classCChessman.html#a7042a12bb4b0dd95eb242d088aa2e929',1,'CChessman']]],
  ['getmovecount',['getMoveCount',['../classCGame.html#a888ad4aaf6356ba0584d73d8dcb31316',1,'CGame']]],
  ['getpath',['getPath',['../classCChessman.html#a9aa5f59cd5de7d70972b404bc2080dc8',1,'CChessman']]],
  ['getplayer',['getPlayer',['../classCGame.html#a5130ddd1155c4b768d2ddbf3f04b6158',1,'CGame']]],
  ['getquadrant',['getQuadrant',['../structTCoord.html#ad5f84892390e071af8719f9cae6b0eb4',1,'TCoord']]]
];
