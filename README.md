# CLChess (téma Šachy) 
Autor: Boris Pankovčin

## Téma z Progtestu

Klasická hra Šachy (příp. libovolná varianta - asijské šachy, ...)

Implementujte následující varianty:

1. pro 2 hráče na jednom počítači
2. pro hru proti počítači
 
Hra musí splňovat následující funkcionality:

1. Dodržování všech pravidel dané varianty (u klasické varianty tedy i rošáda, braní mimochodem, proměna pěšce na dámu).
2. Ukládání (resp. načítání) rozehrané hry do (resp. ze) souboru (vytvořte vhodný formát a uživatelské rozhraní)
3. Oznamovat konec hry (šach, mat, pat) a její výsledek.
4. umělá inteligence (škálovatelná nebo alespoň 3 různé druhy, jeden druh můžou být náhodné tahy, ale nestačí implementovat pouze náhodné tahy)

Kde lze využít polymorfismus? (doporučené)

- Ovládání hráčů: lokální hráč, umělá inteligence (různé druhy), síťový hráč
- Pohyby figurek: král, dáma, věž, kůň,...
- Uživatelské rozhraní: konzolové, ncurses, SDL, OpenGL (různé druhy),...
- Pravidla různých variant: klasické šachy, žravé šachy, asijské šachy
- Jednotlivá pravidla: tahy, rošáda, braní mimochodem, proměna (jejich výběr pak může být konfigurovatelný)

Další informace

- https://cs.wikipedia.org/wiki/Šachy
- https://cs.wikipedia.org/wiki/Šachové_varianty

## Zadanie hry CLChess

Vytvoríme hru "CLChess", ktorá bude fungovať v prostredí terminálu a uživateľ ju bude pomocou jednoduchých príkazov v termináli ovládať.

- `new` vytvorí novú hru, aplikácia sa opýta na mód hry (singleplayer alebo multiplayer), v prípade singleplayer si hráč ešte vyberie úroveň umelej inteligencie a farbu za ktorú chce začínať
- `load <filename>` načíta hru zo súboru
- `save <filename>` uloží hru do súboru
- `move <move descriptor>` presunie figúrku podľa súradníc zadaných v parametri
- `surrender` hráč sa vzdá
- `help ["notation"]` zobrazí nápovedu s príkazmi, prípadne nápovedu k forme zápisu parametrov
- `quit` ukončí program  

Hra dodržuje klasické (európske) pravidlá šachu, teda aj rošády (veľká/malá), branie mimochodom a premenu pešiaka. 
Pre popis pohybu figúrkou bude použitá tzv. čisto súradnicová notácia:
```
<move descriptor> ::= <from square><to square>[promoted to]
<square>          ::= <column letter><row number>
<column letter>   ::= 'a'|'b'|'c'|'d'|'e'|'f'|'g'|'h'
<row number>      ::= '1'|'2'|'3'|'4'|'5'|'6'|'7'|'8'
<promoted to>     ::= 'q'|'r'|'b'|'n'
```

Špeciálny prípad je rošáda pre ktorú sú príkazy `move e1g1` (malá) alebo `move e1b1` (veľká), prípadne `e8g8 / e8b8` pre hráča s čiernymi figúrkami. Teda pre rošádu sa udáva pohyb kráľa.

V prípade premeny pešiaka hráč nemusí zadať typ na ktorý sa má pešiak premeniť, v tom prípade sa pešiak premení implicitne na dámu.

V prípade, že hráč zadá neplatný príkaz alebo nedovolený ťah, hra ho na to upozorní a vyzve aby urobil ťah znova. Obrazovka počas hry môže vyzerať napríklad takto:

```
Black (1) : move d7d5

   a  b  c  d  e  f  g  h
 +------------------------+
8| r  n  b  q  k  b  n  r |8
7| p  p  p  .  p  p  p  p |7
6| .  .  .  .  .  .  .  . |6
5| .  .  .  p  .  .  .  . |5
4| .  .  .  .  .  .  .  . |4
3| .  .  .  .  .  .  .  N |3
2| P  P  P  P  P  P  P  P |2
1| R  N  B  Q  K  B  .  R |1
 +------------------------+
   a  b  c  d  e  f  g  h

White (2) : 
```

O beh aplikácie sa stará trieda `CApplication`, ktorá obsahuje príkazy, ktoré budú implementované ako jednoduché triedy `CCommand`. Okrem toho obsahuje tiež `CInterface`, ktorá obsahuje metody pre prácu so vstupom a výstupom.
Obsahuje tiež `CGame`, ktorá sa stará o prebiehajúcu šachovú partiu a skladá sa z dvoch hráčov (`CPlayer`) a hracej dosky (`CBoard`). 

Trieda `CBoard` obsahuje mapu figúrok (`CChessman`) podľa ich súradníc (`TCoord`) a stará sa o kontrolu ťahu a jeho prevedenie. Obsahuje na to všeobecnú metódu `validateMove`, ktorá sama ešte volá niektoré private metódy kvôli ošetreniu špeciálnych prípadov.

Trieda `CChessman` obsahuje funkciu `m_Path` (volaná metódou `getPath`), ktorá vracia cestu ktorou by figúrka _teoreticky_ išla z jedného políčka na druhé.  

### Kde mám polymorfismus?

Polymorfismus používam pri triede `CPlayer`, ktorá má abstraktnú metódu `requestMove`. 
Implementácie `CPerson` a `CArtificial` preťažujú túto metódu na získanie pohybu figúrky. 
Varianta pre fyzického hráča získa príkaz aj parametre zo vstupného prúdu zatiaľ čo varianta pre umelú inteligenciu vyberie krok na základe algoritmu podľa toho akú má úroveň a vráti `move` (ako string príkaz) a do výstupného parametra zapíše súradnice pohybu. 

Polymorfné volanie je v triede `CInterface`, ktorá si postupne vyžiada od hráčov 1 a 2 aký krok chcú podniknúť.
V závislosti od toho či sa hrá  v móde singleplayer a akej farby je hráč sa na správny `CPlayer` zavolá metóda `requestMove`.

Dynamická väzba na `CPerson/CArtificial` sa vytvorí po volaní príkazu `new`, vrámci vytvárania novej hry.

>Polymorfismus by mohol byť aj nad objektami typu `CCommand` a `CChessman` avšak kvôli zníženiu počtu tried som sa rozhodol použiť radšej lambda funkcie.
